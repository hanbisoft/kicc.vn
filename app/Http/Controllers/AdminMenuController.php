<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Menu;

class AdminMenuController extends Controller
{
    public function index()
    {
        $m = "select * from menu ";
        $menu = DB::select(DB::raw($m));
        return view('backend.menu.index',compact('menu'));
    }

    public function update (Request $request)
    {
        $id = $request->get('get_id');
        // return $id;
        $status = $request->get('status');
        $menu = Menu::find($id);
        $menu->status = $status;
        $menu->save();
        return redirect()->back();
    }
}
