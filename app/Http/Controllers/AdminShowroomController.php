<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\ShowroomEloquentRepository;
use Session;
use App;
use Config;
use Config\Setting;
use App\Model\Showroom;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
class AdminShowroomController extends Controller
{
    protected $roomRepository;

    function __construct(
        ShowroomEloquentRepository $roomRepository
    )
    {
        // parent::__construct();
        $this->roomRepository = $roomRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.showroom.list');

    }

    public function showData()
    {
        $sql = DB::table('showrooms')->select(['id', 'title_en', 'title_ko', 'title_vi','images']);
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($showroom) use ($token) {
                return '
                <a href="/admin/showroom/' . $showroom->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $showroom->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/showroom/' . $showroom->id . '" method="post" id="frm_delete_' . $showroom->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    public function create()
    {
        return view('backend.showroom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = str_random(4) . $image->getClientOriginalName();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(268, 180);
            $image_resize->save(public_path('uploads/' . $filename));
        }
        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else {
            $url_excel = "";
        }
        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf = $request->file('file_pdf')->store('documents');
            $url_pdf = $pdf;
        } else {
            $url_pdf = "";
        }
        $urlFile = 'uploads/' . $filename;

        $slug = str_slug($request->get('title_en'));
        $show = new Showroom;
        $show->title_en = $request->get('title_en');
        $show->title_vi = $request->get('title_vi');
        $show->title_ko = $request->get('title_ko');
        $show->content_en = $request->get('content_en');
        $show->content_vi = $request->get('content_vi');
        $show->content_ko = $request->get('content_ko');
        $show->description_en = $request->get('description_en');
        $show->description_vi = $request->get('description_vi');
        $show->description_ko = $request->get('description_ko');
        $show->images = $urlFile;
        $show->file_excel = $url_excel;
        $show->file_pdf = $url_pdf;
        $show->slug = $slug;
        $show->tel = $request->get('tel');
        $show->email = $request->get('email');
        $show->address = $request->get('address');
        $show->save();
        return redirect()->back()->with('message', 'Create successful!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $show = $this->roomRepository->find($id);
//        return $show;
        return view('backend.showroom.edit', compact('show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $show = Showroom::find($id);
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = str_random(4) . $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(268, 180);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        } else {
            $urlFile = $show->images;
        }
        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else {
            $url_excel = $show->file_excel;
        }
        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf = $request->file('file_pdf')->store('documents');
            $url_pdf = $show->file_pdf;
        } else {
            $url_pdf = "";
        }

        $slug = str_slug($request->get('title_en'));
        $show->title_en = $request->get('title_en');
        $show->title_vi = $request->get('title_vi');
        $show->title_ko = $request->get('title_ko');
        $show->description_en = $request->get('description_en');
        $show->description_vi = $request->get('description_vi');
        $show->description_ko = $request->get('description_ko');
        $show->content_en = $request->get('content_en');
        $show->content_vi = $request->get('content_vi');
        $show->content_ko = $request->get('content_ko');
        $show->images = $urlFile;
        $show->slug = $slug;
        $show->file_excel = $url_excel;
        $show->file_pdf = $url_pdf;
        $show->tel = $request->get('tel');
        $show->email = $request->get('email');
        $show->address = $request->get('address');
        $show->save();
        return redirect()->back()->with('message', 'Update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show = $this->roomRepository->delete($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }
}
