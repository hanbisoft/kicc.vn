<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Menu;
use Illuminate\Support\Facades\DB;
use Session;
use App;
use Config;

class MenuController extends Controller
{
    public function index()
    {

        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';

        }
        if($locale === 'en') {
            $m = "select m.name_en as name, m.url from menu as m where status = 1 limit 7 ";
            $menu = DB::select(DB::raw($m));
        } elseif($locale === 'vi') {
            $m = "select m.name_vi as name, m.url from menu as m where status = 1 limit 7 ";
            $menu = DB::select(DB::raw($m));

        } elseif($locale === 'ko') {
            $m = "select m.name_ko as name, m.url from menu as m where status = 1 limit 7 ";
            $menu = DB::select(DB::raw($m));
        }
        return response()->json($menu);


    }
}
