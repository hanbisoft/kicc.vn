<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\PostEloquentRepository;
use App\Model\Post;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\DB;
use Storage;

class PostController extends Controller
{
    protected $postRepository;

    function __construct(
        PostEloquentRepository $postRepository
    )
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $NEW = 1;
    private $EVENT = 2;

    //new
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $new = Post::where('cate_id', 1)->orderByRaw('created_at DESC')->paginate(5);

        $histories = Post::where('cate_id', 3)->orderByRaw('updated_at - created_at DESC')->paginate(5);
        return view('client.post.new', compact(['new', 'histories', 'locale']));
    }

    public function event(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $year = $request->get('year');
        $month = $request->get('month');


        if (!$year && !$month) {
            $event = Post::where('cate_id', 2)->orderByRaw('created_at DESC')->paginate(10);
        } else {
            $event = Post::where('cate_id', 2)
                ->where('year', $year)
                ->where('month', $month)
                ->get();
        }

        $histories = Post::where('cate_id', 3)->paginate(10);
        return view('client.post.event', compact(['event', 'histories', 'locale']));
    }

    public function showNew($id)
    {
        $new = $this->postRepository->find($id);
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        return view('client.post.detail', compact(['new', 'locale']));
    }


    //function show popup modal
    public function getDataModal(Request $request)
    {
        $id = $request->get('id');
        $locale = $request->get('locale');
        $check_hour_start = "";
        $check_hour_end = "";
        if ($id) {
            $result = $this->postRepository->find($id);
            if ($locale === 'en') {
                return response()->json([
                    'locale' => $locale,
                    'title' => $result->title_en,
                    'description' => $result->description_en,
                    'content' => $result->content_en,
                    'images' => $result->images,
                    'day' => $result->day,
                    'time_start' => $result->time_start,
                    'time_end' => $result->time_end,
                    'link' => $result->link,
                    'month' => $result->month,
                    'file_excel' => $result->file_excel,
                    'file_pdf' => $result->file_pdf,
                    'place' => $result->place
                ]);
            } elseif ($locale === 'vi') {
                return response()->json([
                    'locale' => $locale,
                    'title' => $result->title_vi,
                    'description' => $result->description_vi,
                    'content' => $result->content_vi,
                    'images' => $result->images,
                    'day' => $result->day,
                    'time_start' => $result->time_start,
                    'time_end' => $result->time_end,
                    'link' => $result->link,
                    'month' => $result->month,
                    'file_excel' => $result->file_excel,
                    'file_pdf' => $result->file_pdf,
                    'place' => $result->place
                ]);
            } else {
                return response()->json([
                    'locale' => $locale,
                    'title' => $result->title_ko,
                    'description' => $result->description_ko,
                    'content' => $result->content_ko,
                    'images' => $result->images,
                    'day' => $result->day,
                    'time_start' => $result->time_start,
                    'time_end' => $result->time_end,
                    'link' => $result->link,
                    'month' => $result->month,
                    'file_excel' => $result->file_excel,
                    'file_pdf' => $result->file_pdf,
                    'place' => $result->place
                ]);
            }
        } else {
            return response("Id không tồn tại");
        }
    }

    // function show detail event
    public function showEvent($id)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $event = $this->postRepository->find($id);
        return view('client.post.detail_event', compact(['event', 'locale']));
    }


    //api show event

    public function apiShowEvent(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $year = $request->get('year');
        $month = $request->get('month');

        $e = "select p.* from posts p where cate_id = 2 and  (\"$year\" = '' or concat(p.month) like concat('%', \"$month\", '%') ) limit 10";
        $event = DB::select(DB::raw($e));

        return response()->json([
            "data" => $event
        ]);
    }

    public function download(Request $request)
    {
        return Storage::download($request->get('url'));

    }
}
