<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;
use Config;
use App\Model\Showroom;

class ShowroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $showroom = Showroom::paginate(10);
        return view('client.showroom.index', compact(['locale', 'showroom']));
    }


    public function show($id)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $showroom = Showroom::find($id);
        return view('client.showroom.detail', compact(['locale','showroom']));
    }


}
