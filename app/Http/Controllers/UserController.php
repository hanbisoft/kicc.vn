<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Session;
class UserController extends Controller
{

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import()
    {
        return Excel::import(new UsersImport, 'users.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return response()->json($user);
    }

    public function showDataUser()
    {
        $sql = DB::table('users')->select(['id', 'name', 'email', 'company_name', 'phone',]);
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a  onclick="return showModal(' . $post->id . ')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-minus-sign"></i> Change password</a>';
            })
            ->make(true);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->get('id'));
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->phone = $request->get('phone');
        $user->save();
        return redirect()->back()->with('message', 'Update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json("Xóa thành công");
    }

    public function uploadUser(Request $request)
    {
        $id = Auth::user()->id;
        //upload
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = $image->getClientOriginalName();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(150, 150);
            $image_resize->save(public_path('uploads/' . $filename));
        }
        $urlFile = 'uploads/' . $filename;

        $user = User::find($id);
        $user->images = $urlFile;
        $user->save();
        return redirect()->back();
    }

    public function list()
    {
        $user = User::paginate(10);
        return view('backend.user.index', compact('user'));
    }

    //change pass
    public function changePass(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with('message', 'Update successful!');
    }
}
