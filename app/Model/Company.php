<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'company_name_en',
        'company_name_vi',
        'company_name_ko',
        'images',
        'cate_id',
        'kind',
        'website',
        'address',
        'tel1',
        'otel',
        'email1',
        'email2',
        'class1',
        'class2',
        'sales1',
        'sales2',
        'about_us_en',
        'about_us_ko',
        'about_us_vi',
        'mtel',
        'manger',
        'area',
        'type',
        'date',
        'emp_ko',
        'emp_VN',
        'location',
        'owmer',
        'file_excel',
        'service_en',
        'service_ko',
        'service_vi',
        'file_pdf',

    ];
}
