<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
    protected $fillable = [
        'name',
        'link_file',
        'type',
        'post_id',
        'company_id',
        'showroom_id'
    ];
}
