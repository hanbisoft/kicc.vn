<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title_en',
        'title_vi',
        'title_ko',
        'content_en',
        'content_vi',
        'content_ko',
        'images',
        'cate_id',
        'description_en',
        'description_vi',
        'description_ko',
        'slug',
        'day',
        'month',
        'time_start',
        'end_start',
        'link',
        'file_excel',
        'file_pdf',
        'link',
        'year',
        'created_at',
        'end_day',
        'place'
    ];
}
