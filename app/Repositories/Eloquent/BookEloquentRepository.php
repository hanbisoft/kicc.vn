<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Model\BookRoom;

class BookEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return BookRoom::class;
    }

}
