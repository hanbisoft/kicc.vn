<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Model\Post::class, function (Faker $faker) {
    $title = $faker->text($maxNbChars = 50);
    $slug = str_slug($title);
    return [
        'slug'=> $slug,
        'title_en' => $title,
        'description_en' =>   $faker->text($maxNbChars = 190),
        'content_en' => $faker->text($maxNbChars = 1000),
        'title_vi' => $title,
        'description_vi' =>   $faker->text($maxNbChars = 190),
        'content_vi' =>  $faker->text($maxNbChars = 1000),
        'title_ko' => $title,
        'description_ko' =>  $faker->text($maxNbChars = 190),
        'content_ko' => $faker->text($maxNbChars = 1000),
        'images'=>'uploads/new2.jpg',
        'cate_id'=> 3,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,

    ];
});

//'day'=> '03',
//        'link'=>'',
//        'time_start'=>'11:43:51',
//        'time_end'=>'13:44:07',
//        'month'=>'02'