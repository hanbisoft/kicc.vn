<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Model\Showroom::class, function (Faker $faker) {
    $title = $faker->text($maxNbChars = 60);
    $slug = str_slug($title);
    return [
        'slug'=>$slug,
        'title_en' => $title,
        'content_en' => $faker->text($maxNbChars = 10000),
        'title_vi' => $title,
        'content_vi' => $faker->text($maxNbChars = 10000),
        'title_ko' => $title,
        'content_ko' => $faker->text($maxNbChars = 10000),
        'description_vi' => $faker->text($maxNbChars = 190),
        'description_en' =>  $faker->text($maxNbChars = 190),
        'description_ko' =>  $faker->text($maxNbChars = 190),
        'images'=>'uploads/showroom3.jpg',
        'images1'=>'uploads/showroom4.jpg',
        'images2'=>'uploads/showroom3.jpg',
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ];
});
