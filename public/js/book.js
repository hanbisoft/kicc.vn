var dataResult = [];
$.ajax({
    url: "{{ route('client.book.show') }}",
    beforeSend: function() { $('#wait').show(); },
    complete: function() { $('#wait').hide(); },
    type: "get",
    dateType: "json",
    success: function (result) {
        dataResult = result;
        $('#calendar').fullCalendar({
            defaultView:'month',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            timeFormat:'H:mm',
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                var date_default= start;
                console.log(date_default.format())
                $('#start_date').val(date_default.format());
                $('#myModal').modal('show')
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: dataResult,
            eventColor: '#ffffff',
            loading: function (bool) {
                $('#loading').toggle(bool);
            }
        });
    }
});
function show() {
    $body = $("body");


    var user_id = document.getElementById('user_id').value;
    var company_name = document.getElementById('company_name').value;
    var room_id = document.getElementById('room_id').value;
    var start_date = document.getElementById('start_date').value;
    var time_start = document.getElementById('time_start').value;
    var time_end = document.getElementById('time_end').value;
    var note = document.getElementById('note').value;
    var qty = document.getElementById('qty').value;
    $.ajax({
        url: "{{ route('client.book.new') }}",
        beforeSend: function() { $('#wait').show(); },
        complete: function() { $('#wait').hide(); },
        type: "get",
        data: {
            'user_id': user_id,
            'company_name': company_name,
            'room_id': room_id,
            'start_date': start_date,
            'time_start': time_start,
            'time_end': time_end,
            'qty': qty,
            'note': note
        },
        dateType: "text",
        success: function (result) {
            console.log(result)
            if(result.code === 200) {
                location.href='{{route('book.confirm')}}'
            } else  {
                console.log(result)
                $('#mess_error').html("").html(result.mess)
            }

        }
    });
}