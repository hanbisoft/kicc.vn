jQuery(document).ready(function ($) {
    $('.slider-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        autoplay:true,
        autoplayTimeout:5000,
        dots: true,
        autoHeight: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {
    $('.new-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight:false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },  
            992:{
                items:2,
                nav:true
            },  
            1200:{
                items:3,
                nav:true
            }        
        }
    })
});  

   
jQuery(document).ready(function ($) {
    $('.incubat-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },  
            992:{
                items:2,
                nav:true
            },  
            1200:{
                items:3,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {
    $('.showroom-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },
            768:{
                items:2,
                nav:true
            },  
            992:{
                items:3,
                nav:true
            },  
            1200:{
                items:4,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {
    $('.match-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },
            768:{
                items:2,
                nav:true
            },
            1200:{
                items:3,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {
    $('.history-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },
            768:{
                items:2,
                nav:true
            },
            1200:{
                items:3,
                nav:true
            }        
        }
    })  
}); 
jQuery(document).ready(function ($) {
    $('.share-inner.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: true,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:1,
                nav:true
            },
            768:{
                items:2,
                nav:true
            },
            1200:{
                items:3,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {
    $('.brand-hb.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: false,
        autoHeight: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        navClass: ['owl-prev', 'owl-next'],
        responsiveClass:true,
        responsive:{
            0:{ 
                items:2,
                nav:true
            },
            768:{
                items:4,
                nav:true
            },
            992:{
                items:5,
                nav:true
            },
            1200:{
                items:7,
                nav:true
            }        
        }
    })
});
jQuery(document).ready(function ($) {    
                            
});
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
});
jQuery(document).ready(function ($) {
    var $window = $(window); 
        function check_if_in_view() {
            var $animation_elements = $('.animation-element');
            var window_height = $window.height();
            var window_top_position = $window.scrollTop();
            var window_bottom_position = (window_top_position + window_height); 

        $.each($animation_elements, function(key,item) {
            var $element = $(item);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);
            if ((element_top_position <= window_bottom_position)) {

              $element.addClass('in-view');
            } else {
              $element.removeClass('in-view');
            }
        });
    } 
    $window.on('scroll resize', check_if_in_view);    
    $window.trigger('scroll');
});