function show($id) {
    var locale = document.getElementById('locale').value;
    var title = '';
    var images = '';
    var content = '';
    var socical = '';
    $.ajax({
        url: "{{ route('api.new') }}",
        type: "get",
        data: {
            'locale': locale,
            'id': $id
        },
        dateType: "text",
        success: function (result) {
            console.log(result)
            title += result.title;
            images += "<img   class='img-fluid' src='" + result.images + "'>";
            content += result.content;
            $("#exampleModalLongTitle").html(title);
            $("#title").html(title);
            $("#img").html(images);
            $("#content").html("<p class='description'>"+content+"</p>");
            $('#new').modal('show')
        }
    });
}

function showEvent($id) {
    var locale = document.getElementById('locale').value;
    var title = '';
    var images = '';
    var content = '';
    var socical = '';
    $.ajax({
        url: "{{ route('api.new') }}",
        type: "get",
        data: {
            'locale': locale,
            'id': $id
        },
        dateType: "text",
        success: function (result) {
            console.log(result)
            title += result.title;
            images += "<img   class='img-fluid' src='" + result.images + "'>";
            content += result.content;
            $("#exampleModalLongTitle").html(title);
            $("#title").html(title);
            $("#img").html(images);
            $("#content").html(content);
            $('#exampleModalLong').modal('show')
        }
    });
}