<?php

return [
    'event' => 'Event',
    'news' => 'News',
    'incubating' => 'Incubating',
    'showroom' => 'Showroom',
    'b2b_matching' => 'B2B Matching',
    'history' => 'History',
    'share_room' => 'Reservation (Free)',
    'company_infomation'=> 'Company Infomation',
    'company' =>'Company',
    'support'=>'Support',
    'about'=>'About Us',
    'login'=>'Login',
    'register'=>'Register',
    'view_all'=>'View All',
    'search'=>'Search',
    'key_search'=>'Please enter your details',
    'contact' =>'Contact us',
    'asean' => 'ASEAN',
    'book_shareroom' => 'Book Shareroom Free',
    'new_event'=> 'News & Event',
    'search'=>'Search',
    'search_content'=> 'Please enter your details'
];
