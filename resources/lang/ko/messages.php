<?php

return [
    'event' => '사건',
    'news' => '뉴스',
    'incubating' => '인큐베이팅',
    'showroom' => '쇼룸',
    'b2b_matching' => 'B2B 매칭',
    'history' => '연혁',
    'share_room' => '쉐어룸(무료)',
    'company_infomation'=> '회사 정보',
    'company' =>'Company',
    'support'=>'지원',
    'about'=>'회사 소개',
    'login'=>'로그인',
    'register'=>'회원가입',
    'view_all'=>'모두 보기',
    'search'=>'검색',
    'key_search'=>'내용를 입력하십시오',
    'contact' => 'Contact Us',
    'asean' => '아세안',
    'book_shareroom' => 'Book Shareroom Free',
    'new_event'=> 'News & Event',
    'search'=>'조회',
    'search_content'=> '내용을 입력해주세요'
];
