<?php

return [
    'event' => 'Sự kiện',
    'news' => 'Tin tức',
    'incubating' => 'DN ươm tạo',
    'showroom' => 'Phòng trưng bày',
    'b2b_matching' => 'Kn giao thương',
    'history' => 'Lịch sử',
    'share_room' => 'Phòng họp',
    'company_infomation'=> 'Công ty',
    'support'=>'Hỗ trợ',
    'about'=>'Về chúng tôi',
    'login'=>'Đăng nhập',
    'register'=>'Đăng ký',
    'view_all'=>'Xem tất cả',
    'search'=>'Tìm kiếm',
    'key_search'=>'Vui lòng nhập nội dung của bạn',
    'contact'=>'Liên Hệ',
    'asean' => 'ASEAN',
    'company' =>'Công ty',
    'book_shareroom' => 'Đặt phòng họp miễn phí',
    'new_event'=> 'Tin tức & Sự kiện',
    'search'=>'Tìm kiếm',
    'search_content'=> 'Vui lòng nhập nội dung của bạn'
];
