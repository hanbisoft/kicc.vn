@extends('master')
@section('style')
    <link rel="stylesheet" href="{{asset('stylelogin/fonts/material-icon/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('stylelogin/css/style.css')}}">
@endsection
@section('content')
    <div class="body">
        <div class="main">

            <section class="signup">
                <!-- <img src="images/signup-bg.jpg" alt=""> -->
                <div class="container23">
                    <div class="signup-content">
                        <form  id="signup-form" class="signup-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <h2 class="form-title">Create account</h2>
                            <div class="form-group">
                                <input type="text" class="form-input {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" placeholder="Name" value="{{ old('name') }}" required autofocus/>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-input {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" id="company_name" placeholder="Company Name" value="{{ old('company_name') }}"/>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Your Email" value="{{ old('email') }}" required/>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-input {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="phone" placeholder="Your phone" value="{{ old('phone') }}" />
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-input {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" placeholder="Password"/>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <input type="password"  required class="form-input" name="password_confirmation" id="password_confirmation" placeholder="Repeat your password"/>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" id="submit" class="form-submit" value="Sign up"/>
                            </div>
                        </form>
                        <p class="loginhere">
                            Have already an account ? <a href="{{route('login')}}" class="loginhere-link">Login here</a>
                        </p>
                    </div>
                </div>
            </section>

        </div>
    </div>
@endsection

