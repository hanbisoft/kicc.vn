@extends('backend.master_admin')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Update Company</h2>
        <br>
        <form action="{{route('company.update', ['id' => $company->id])}}" enctype="multipart/form-data" method="POST"
              style="margin-bottom: 2em">
            {{ method_field('PUT') }}
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Images</label>
                        <input value="uploads/noimage.jpg" type="file" class="from-control" name="img" id="img">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <img src="{{asset($company->images)}}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Category</label>
                    <select required name="cate_id" class="form-control" id="cate_id">
                        @if($company->cate_id === 2)
                            <option value="2">Incubating</option>
                            <option value="3">B2b matching</option>
                        @elseif($company->cate_id === 3)
                            <option value="3">B2b matching</option>
                            <option value="2">Incubating</option>
                        @endif
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>File Pdf</label>
                        <input accept="application/pdf,application/vnd.ms-excel" type="file"
                               class="from-control" name="file_pdf" id="file_pdf">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>File Excel</label>
                        <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                               type="file" class="from-control" name="file_excel" id="file_excel">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Eng)</label>
                        <input required class="form-control" type="text" value="{{$company->company_name_en}}"
                               name="company_name_en">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Ko)</label>
                        <input class="form-control" type="text" value="{{$company->company_name_ko}}"
                               name="company_name_ko">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Vi)</label>
                        <input class="form-control" type="text" value="{{$company->company_name_vi}}"
                               name="company_name_vi">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company tel</label>
                        <input class="form-control" type="text" value="{{$company->tel1}}" name="tel1">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kind</label>
                        <input class="form-control" type="text" value="{{$company->kind}}" name="kind">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date</label>
                        <input class="form-control" type="date" value="{{$company->date}}" name="date">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company type</label>
                        <input class="form-control" type="text" value="{{$company->type}}" name="type">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Owner name</label>
                        <input class="form-control" type="text" value="{{$company->owmer}}" name="owmer">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Owner tel</label>
                        <input class="form-control" type="text" value="{{$company->otel}}" name="otel">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Manager</label>
                        <input class="form-control" type="text" value="{{$company->manger}}" name="manger">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Manager tel</label>
                        <input class="form-control" type="text" value="{{$company->mtel}}" name="mtel">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email 1</label>
                        <input class="form-control" type="text" value="{{$company->email1}}" name="email1">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email 2</label>
                        <input class="form-control" type="text" value="{{$company->email2}}" name="email2">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Website</label>
                        <input class="form-control" type="text" value="{{$company->website}}" name="website">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>About us En</label>
                        <textarea class="form-control" name="about_us_en">
                        {{$company->about_us_en}}
                    </textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>About us Ko</label>
                        <textarea class="form-control"  name="about_us_ko">
                            {{$company->about_us_ko}}
                        </textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>About us Vi</label>
                        <textarea class="form-control"  name="about_us_vi">
                            {{$company->about_us_vi}}
                        </textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Number of korean employee</label>
                        <input class="form-control" type="text" value="{{$company->emp_ko}}" name="emp_ko">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Number of Viet Nam employee</label>
                        <input class="form-control" type="text" value="{{$company->emp_VN}}" name="emp_VN">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Area</label>
                        <input class="form-control" type="text" value="{{$company->area}}" name="area">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Location</label>
                        <input class="form-control" type="text" value="{{$company->location}}" name="location">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" type="text" value="{{$company->address}}" name="address">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Eng)</label>
                        <input class="form-control" type="text" value="{{$company->service_en}}" name="service_en">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Vi)</label>
                        <input class="form-control" type="text" value="{{$company->i}}" name="service_vi">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Ko)</label>
                        <input class="form-control" type="text" value="{{$company->service_ko}}" name="service_ko">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-danger">Update</button>
                </div>
            </div>
        </form>
    </div>
@endsection
