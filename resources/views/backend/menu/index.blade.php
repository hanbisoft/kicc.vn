@extends('backend.master_admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>List Menu</h2>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-company">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name (Eng)</th>
                <th>Name (Vi)</th>
                <th>Name (Ko)</th>
                <th>Url</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($menu as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name_en }}</td>
                    <td>{{ $value->name_vi }}</td>
                    <td>{{ $value->name_ko }}</td>
                    <td>{{ $value->url }}</td>
                    <td>
                        @if($value->status === 1)
                            <form action="{{route('admin.menu-update')}}" method="GET" id="frm_disable">
                                    <input type="hidden" name="status" value="2">
                                    <input type="hidden" name="get_id" value="{{ $value->id }}">
                                    <button type="submit"  class="btn btn-sm btn-success">Active</button>
                            </form>

                        @else
                            <form action="{{route('admin.menu-update')}}" method="GET" id="frm_active">
                                <input type="hidden" name="status" value="1">
                                <input type="hidden" name="get_id" value="{{ $value->id }}">
                                <button type="submit" class="btn btn-sm btn-danger">Disable</button>
                            </form>

                        @endif
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

@endsection
@push('script')

@endpush
