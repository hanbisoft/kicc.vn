@extends('backend.master_admin')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Create Event</h2>
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#home"> <img src="{{asset('images/eng.png')}}" alt="">English</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu1"><img src="{{asset('images/vie.png')}}" alt="">Việt Nam</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu2"><img src="{{asset('images/kor.png')}}" alt="">Korean</a>
            </li>
        </ul>
        <form action="{{route('post.store')}}" enctype="multipart/form-data" method="post" style="margin-bottom: 2em">
            @csrf
            <input type="hidden" value="2" name="cate_id">
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input required class="form-control" placeholder="title" name="title_en" id="title_en">
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <input required type="file" class="from-control" name="img" id="img">
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Pdf</label>
                                <input accept="application/pdf,application/vnd.ms-excel" type="file"
                                       class="from-control" name="file_pdf" id="file_pdf">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Excel</label>
                                <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                       type="file" class="from-control" name="file_excel" id="file_excel">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Day</label>
                            <input type="number" placeholder="Please enter day start event" class="form-control "
                                   name="day" id="day">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Day end event</label>
                            <input type="date" name="end_day" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Month</label>
                            <select required name="month" id="month" class="form-control">
                                <option value="">Please select</option>
                                <option value="Jan"> January</option>
                                <option value="Feb">February</option>
                                <option value="Mar"> March</option>
                                <option value="Apr"> April</option>
                                <option value="May"> May</option>
                                <option value="Jun"> June</option>
                                <option value="Jul">July</option>
                                <option value="Aug">August</option>
                                <option value="Sep">September</option>
                                <option value="Oct">October</option>
                                <option value="Nov">November</option>
                                <option value="Dev">December</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Year</label>
                            <select class="form-control" name="year" id="year">
                                <option value="">Please select</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Time start</label>
                            <input type="text" name="time_start" class="form-control" placeholder="12:00:00" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Time end</label>
                            <input type="text" name="time_end" class="form-control" placeholder="17:30:00" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_en" name="content_en" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Place</label>
                        <input type="text" value="" placeholder="Place..." class="form-control" name="place" id="place">
                    </div>
                    <div class="form-group">
                        <label>Create date</label>
                        <input type="date" value="{{ date("Y-m-d") }}" class="form-control" name="created_at"
                               id="created_at">
                    </div>
                </div>
                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" placeholder="title" name="title_vi" id="title_vi">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_vi" name="content_vi" class="form-control"></textarea>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" placeholder="title" name="title_ko" id="title_ko">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_ko" name="content_ko" class="form-control"></textarea>
                    </div>
                </div>

                <button style="margin-left: 1em" type="submit" class="btn btn-danger">Create</button>
                <br>
            </div>

        </form>
    </div>

    <script>
        $(document).ready(function () {
            $(".nav-tabs a").click(function () {
                $(this).tab('show');
            });
        });

        for (i = new Date().getFullYear(); i > 2015; i--) {
            $('#year').append($('<option />').val(i).html(i));
        }
    </script>


@endsection
