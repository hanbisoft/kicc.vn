@extends('backend.master_admin')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Update Event</h2>
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#home"> <img src="{{asset('images/eng.png')}}" alt="">English</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu1"><img src="{{asset('images/vie.png')}}" alt="">Việt Nam</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu2"><img src="{{asset('images/kor.png')}}" alt="">Korean</a>
            </li>
        </ul>
        <form action="{{route('post.update', ['id' => $event->id])}}" enctype="multipart/form-data" method="POST"
              style="margin-bottom: 2em">
            {{ method_field('PUT') }}
            @csrf
            <input type="hidden" name="cate_id" value="2">
        <!-- Tab panes -->
            <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$event->title_en}}" class="form-control" placeholder="title" name="title_en"
                               id="title_en">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" class="from-control" name="img" id="img">
                            </div>

                        </div>
                        <div class="col-md-8">
                            <img style="width: 80px" src="{{asset($event->images)}}" alt="">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Pdf</label>
                                <input accept="application/pdf,application/vnd.ms-excel" type="file"
                                       class="from-control" name="file_pdf" id="file_pdf">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Excel</label>
                                <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                       type="file" class="from-control" name="file_excel" id="file_excel">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Day</label>
                            <input value="{{$event->day}}" type="number" placeholder="Please enter day start event" class="form-control " name="day" id="day">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Day end event</label>
                            <input type="date" name="end_day" class="form-control" value="{{$event->end_day}}">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Month</label>
                            <select required name="month" id="month" class="form-control">
                                <option value="{{$event->month}}">{{$event->month}}</option>
                                <option value="Jan"> January</option>
                                <option value="Feb">February</option>
                                <option value="Mar"> March</option>
                                <option value="Apr"> April</option>
                                <option value="May"> May</option>
                                <option value="Jun"> June</option>
                                <option value="Jul">July</option>
                                <option value="Aug">August</option>
                                <option value="Sep">September</option>
                                <option value="Oct">October</option>
                                <option value="Nov">November</option>
                                <option value="Dev">December</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Year</label>
                            <select class="form-control" name="year" id="year">
                                <option selected value="{{$event->year}}">{{$event->year}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Time start</label>
                            <input  value="{{$event->time_start}}" type="text" name="time_start" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Time end</label>
                            <input value="{{$event->time_end}}" type="text" name="time_end" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_en" name="content_en" class="form-control">{{$event->content_en}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" placeholder="title" name="title_vi" id="title_vi">
                    </div>
                    <div class="form-group">
                        <label>Create date</label>
                        <input type="date" value="{{$event->created_at->todatestring()}}" class="form-control" name="created_at" id="created_at">
                    </div>
                    <div class="form-group">
                        <label>Place</label>
                        <input type="text" value="{{$event->place}}" placeholder="Place..." class="form-control" name="place" id="place">
                    </div>
                </div>
                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$event->title_vi}}" class="form-control" placeholder="title" name="title_vi"
                               id="title_vi">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_vi" name="content_vi" class="form-control">{{$event->content_vi}}</textarea>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$event->title_ko}}" class="form-control" placeholder="title" name="title_ko"
                               id="title_ko">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_ko" name="content_ko"
                                  class="form-control"> {{$event->content_ko}}</textarea>
                    </div>
                </div>
                <button style="margin-left: 1em" type="submit" class="btn btn-danger">Update</button>
                <br>
            </div>

        </form>
    </div>

<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
    });
    for (i = new Date().getFullYear(); i > 2015; i--)
    {
        $('#year').append($('<option />').val(i).html(i));
    }
</script>

@endsection