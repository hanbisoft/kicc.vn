@extends('backend.master_admin')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Update Show room</h2>
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#home"> <img src="{{asset('images/eng.png')}}" alt="">English</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu1"><img src="{{asset('images/vie.png')}}" alt="">Việt Nam</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu2"><img src="{{asset('images/kor.png')}}" alt="">Korean</a>
            </li>
        </ul>
        <form action="{{route('showroom.update', ['id' => $show->id])}}" enctype="multipart/form-data" method="POST"
              style="margin-bottom: 2em">
        {{ method_field('PUT') }}
        @csrf
        <!-- Tab panes -->
            <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$show->title_en}}" class="form-control" placeholder="title" name="title_en"
                               id="title_en">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" class="from-control" name="img" id="img">
                            </div>

                        </div>
                        <div class="col-md-8">
                            <img style="width: 80px" src="{{asset($show->images)}}" alt="">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Pdf</label>
                                <input accept="application/pdf,application/vnd.ms-excel" type="file" class="from-control" name="file_pdf" id="file_pdf">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Excel</label>
                                <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                       type="file" class="from-control" name="file_excel" id="file_excel">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_en" name="content_en" class="form-control">{{$show->content_en}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Tel</label>
                        <input type="text" class="form-control" name="tel" placeholder="Tel..." value="{{$show->tel}}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email..." value="{{$show->email}}">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" placeholder="Address..." value="{{$show->address}}">
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description_en" id="description_en" class="form-control">
                            {{$show->description_en}}
                        </textarea>
                    </div>
                </div>

                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$show->title_vi}}" class="form-control" placeholder="title" name="title_vi"
                               id="title_vi">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_vi" name="content_vi"
                                  class="form-control">{{$show->content_vi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description_vi" id="description_vi" class="form-control">
                            {{$show->description_vi}}
                        </textarea>
                    </div>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="{{$show->title_ko}}" class="form-control" placeholder="title" name="title_ko" id="title_ko">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_ko" name="content_ko" class="form-control"> {{$show->content_ko}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description_ko" id="description_ko" class="form-control">
                            {{$show->description_ko}}
                        </textarea>
                    </div>
                </div>
                <button style="margin-left: 1em" type="submit" class="btn btn-danger">Update</button>
                <br>
            </div>

        </form>
    </div>

    <script>
        $(document).ready(function () {
            $(".nav-tabs a").click(function () {
                $(this).tab('show');
            });
        });
        CKEDITOR.replace('content_vi', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('content_en', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('content_ko', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

        CKEDITOR.replace('description_vi', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('description_en', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('description_ko', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>

@endsection