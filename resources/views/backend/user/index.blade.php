@extends('backend.master_admin')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>List User</h2>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-company">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Company name</th>
                <th>Phone</th>
                <th colspan="2"></th>
            </tr>
            </thead>
        </table>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.changepass')}}" method="POST">
                    @csrf
                    <input type="hidden" value="" id="user_id" name="user_id">
                    <div class="modal-body">

                        <input class="form-control" type="text" value="abc@kicc" name="password">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')

    <script>
        $(function () {
            $('#dataTables-company').DataTable({
                async: true,
                ajax: {
                    "url": "{{ route('api.list-user') }}",
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    {data: 'id'},
                    {data: 'name', orderable: true, searchable: true},
                    {data: 'email'},
                    {data: 'company_name'},
                    {data: 'phone'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });

        function showModal($id) {

            $('#myModal').modal('show')
            var input = $("#user_id");
            input.val($id);
        }
    </script>
@endpush
