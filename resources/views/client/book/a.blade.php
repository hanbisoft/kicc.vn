<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Reservation</title>

    <style type="text/css">
        p, body, td, input, select, button { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; }
        body { padding: 0px; margin: 0px; background-color: #ffffff; }
        a { color: #1155a3; }
        .space { margin: 10px 0px 10px 0px; }
        .header { background: #003267; background: linear-gradient(to right, #011329 0%,#00639e 44%,#011329 100%); padding:20px 10px; color: white; box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.75); }
        .header a { color: white; }
        .header h1 a { text-decoration: none; }
        .header h1 { padding: 0px; margin: 0px; }
        .main { padding: 10px }
        select, option { padding: 4px; }
        button {
            background-color: #3c78d8;
            border: 1px solid #1155cc;
            color: #fff;
            padding: 6px 20px;
            border-radius: 2px;
            cursor: pointer;
        }
    </style>

    <!-- DayPilot library -->
    <script src="js/daypilot/daypilot-all.min.js"></script>
    <script src='{{asset('calender/lib/jquery.min.js')}}'></script>
    <!-- jsPDF: PDF generator -->
    <script src="js/jspdf/jspdf.min.js"></script>

</head>
<body>
<div class="main">
    <h2>Reservation</h2>
    <div id="dp"></div>
    <div class="space">
        <h2>Export to PDF</h2>
        <select id="pdf-orientation">
            <option value="landscape">Landscape</option>
        </select>
        <select id="pdf-format">
            <option value="a4">A4</option>
        </select>
        <button id="export">Export</button>
    </div>
</div>

<script>
    function SubtractDays(toAdd) {
        if (!toAdd || toAdd == '' || isNaN(toAdd)) return;
        var d = new Date();
        d.setDate(d.getDate() - parseInt(toAdd));

        document.getElementById("result").innerHTML = d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear();
    }

    Date.prototype.addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    var dataResult = [];
    var dataUpdate = [];
    this.callApi();

    //function get api
    function callApi() {
        $.ajax({
            url: "/api/book-pdf",
            beforeSend: function () {
                $('#wait').show();
            },
            complete: function () {
                $('#wait').hide();
            },
            type: "get",
            dateType: "json",
            success: function (result) {
                dataUpdate = result.map(x=>x);
                //update list
                setCalendar(dataUpdate)
            }
        });
    }
    function setCalendar(dataUpdate)
    {
        console.log(dataUpdate)
        var dp = new DayPilot.Calendar("dp", {
            locale: "en-us",
            viewType: "Week",
            columnWidthSpec: "Auto",
            cellHeight: 20,
            crosshairType: "Header",
            businessBeginsHour: 9,
            businessEndsHour: 17,
            dayBeginsHour: 9,
            dayEndsHour: 18,
            eventArrangement: "Cascade",
            timeRangeSelectedHandling: "Disabled",
            eventMoveHandling: "Disabled",
            eventDeleteHandling: "Disabled",
            eventClickHandling: "Disabled",
            eventHoverHandling: "Disabled"
        });
        dp.events.list =dataUpdate;

        dp.init();
        console.log(DayPilot.Date.today().addHours(11))
        var pdfConfig = {
            "letter-portrait": {
                orientation: "portrait",
                unit: "in",
                format: "letter",
                maxWidth: 7,
                maxHeight: 9,
                left: 0.5,
                top: 1,
                space: 0.2
            },
            "letter-landscape": {
                orientation: "landscape",
                unit: "in",
                format: "letter",
                maxWidth: 9,
                maxHeight: 7,
                left: 0.5,
                top: 1,
                space: 0.2
            },
            "a4-portrait": {
                orientation: "portrait",
                unit: "cm",
                format: "a4",
                maxWidth: 18,
                maxHeight: 23,
                left: 1.5,
                top: 3,
                space: 0.5
            },
            "a4-landscape": {
                orientation: "landscape",
                unit: "cm",
                format: "a4",
                maxWidth: 23,
                maxHeight: 18,
                left: 1.5,
                top: 3,
                space: 0.5
            }
        };

        (function () {
            document.getElementById("export").addEventListener("click", function (e) {
                var orientation = document.getElementById("pdf-orientation").value;
                var format = document.getElementById("pdf-format").value;
                var config = pdfConfig[format + "-" + orientation];
                var blob = createPdfAsBlobOnePage(config);
                DayPilot.Util.downloadBlob(blob, "calendar.pdf");
            });
        })();

        function createPdfAsBlobOnePage(config) {
            var pdf = new jsPDF(config.orientation, config.unit, config.format);
            pdf.setFontSize(20);
            pdf.text(config.left, config.top, "Reservation");

            var image = dp.exportAs("jpeg", {
                scale: 2,
                quality: 0.95
            });

            var dimensions = image.dimensions();  // pixels
            var maxDimensions = {width: config.maxWidth, height: config.maxHeight};   // inches
            var adjusted = shrink(dimensions, maxDimensions);

            pdf.addImage(image.toDataUri(), 'JPEG', config.left, config.top + config.space, adjusted.width, adjusted.height);

            return pdf.output("blob");
        }

        function shrink(dimensions, max) {
            var widthRatio = dimensions.width / max.width;
            var heightRatio = dimensions.height / max.height;

            var ratio = Math.max(widthRatio, heightRatio);
            ratio = Math.max(ratio, 1);

            var width = dimensions.width / ratio;
            var height = dimensions.height / ratio;
            return {width: width, height: height};
        }
    }


</script>

</body>
</html>
