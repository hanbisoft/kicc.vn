@extends('master')
@section('content')
        <main>
            <div id="slider-nb" class="slider-nb"></div>
            <section class="section-hb incubat-hb animation-element slide-left">
                <div class="container">
                    <div class="content">
                       <div class="row">
                           <h5 ><a class="text-success" href="">Booking complete, please check your email address: {{$book->email}}</a></H5>
                           <table class="table table-bordered" style="margin-top: 2em">
                               <tr>
                                   <td>Room</td>
                                   <td>{{$book->room_id}}</td>
                               </tr>
                               <tr>
                                   <td>Time Start</td>
                                   <td>{{$book->start_date}}</td>
                               </tr>
                               <tr>
                                   <td>Time End</td>
                                   <td>{{$book->end_date}}</td>
                               </tr>
                           </table>
                            <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has </p>
                           </div>
                        <div style="margin: 0 auto;text-align: center">
                            <a href="/book/1" class="btn btn-primary" style="height: auto;background: #204181;color: #fff;width: 100px; -webkit-border-radius: 3rem; -moz-border-radius: 3rem;border-radius: 3rem;margin-top:3em">Back</a>
                        </div>
                       </div>
                    </div>
            </section>
        </main>
@endsection
