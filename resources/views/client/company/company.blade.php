@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('searchCompany')}}" method="GET">
                        <input class="form-control mr-sm-2" type="search" name="search" placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="heading">
                            <div class="heading-left">
                                <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                                <h3>{{ trans('messages.incubating') }}</h3>
                            </div>

                        </div>
                        <div class="content">
                            <div class=" row" style="padding-left: inherit">
                                @foreach($incubating as $value)
                                    <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view"
                                         style="width: 353.333px;margin-right: auto;padding:0;margin-bottom: 1em">
                                        <a href="/company/{{$value->id}}"><div style="max-height: 233.525px" class="incubat-img new-img"><img style="width: 100%" src="{{asset($value->images)}}" alt=""></div></a>
                                        <div class="incubat-text caption">
                                            @if($locale === 'en')
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @else
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="heading">
                            <div class="heading-left">
                                <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                               <h3>{{ trans('messages.b2b_matching') }}</h3>
                                <form action="{{route('searchB2b')}}" method="GET" style="margin-left: 30em">
                                    <div class="row">
                                        <div class="input-group">
                                            <input class="form-control py-2" type="search" placeholder="search"
                                                   id="search" name="search">
                                            <span class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="heading-right">

                                <a href="{{route('company.b2b')}}" class="btn btn-hb btn-viewall">View all</a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="incubat-inner owl-carousel">
                                @foreach($b2b_matching as $value)
                                        <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view">
                                            <a href="/company/{{$value->id}}"><div style="max-height: 233.525px" class="incubat-img new-img"><img src="{{asset($value->images)}}" alt=""></div></a>
                                            <div class="incubat-text caption">
                                                @if($locale === 'en')
                                                    <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                                    <p class="description">{{$value->address}}</p>
                                                @elseif($locale === 'vi')
                                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                                        <p class="description">{{$value->address}}</p>
                                                @else
                                                    <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                                    <p class="description">{{$value->address}}</p>
                                                @endif
                                            </div>
                                        </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </section>
                </div>
        </section>
    </main>

@endsection
