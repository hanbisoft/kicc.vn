@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('searchCompany')}}" metdod="GET">
                        <input name="search" class="form-control mr-sm-2" type="search"
                               placeholder="{{ trans('messages.search') }}"
                               aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content">
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="col-md-2">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table2">
                            <tr>
                                <td style="background:none; width: 50px;height: 50px" scope="row" class="bg text-center"><img class="img-responsive" src="{{asset($company->images)}}"  alt="">
                                </td>
                                <td  style="vertical-align: inherit">
                                    <h2 style="color:#204181;font-weight: bold; font-size:35px " class="text-center"> {{$company->company_name_ko}}</h2>
                                    <h3 style="text-align:center; padding-top:5px;font-size:20px">{{$company->company_name_en}}</h3>
                                </td>
                            </tr>

                            <tr>
                                <td  class="bg text-center">Web Site</td>
                                <td  class="bg2"><a target="_blank" href="{{ $company->website }}">{{ $company->website }}</a></td>
                                <td>@if($company->file_excel != null || $company->file_pdf != null)
                                    <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                                @endif</td>
                            </tr>

                            <tr>
                                <td  class="bg text-center">Kind</td>
                                <td  class="bg2">{{$company->kind}}</td>
                            </tr>
                            <tr>
                                <td  class="text-center bg">Company tel</td>
                                <td class="bg2">{{$company->tel1}}</td>

                            </tr>
                            <tr>
                                    <td  class="text-center bg">Email 1</td>
                                    <td class="bg2">{{$company->email1}}</td>

                            </tr>
                            <tr>
                                    <td  class="text-center bg">Email 2</td>
                                    <td class="bg2">{{$company->email2}}</td>

                            </tr>
                            <tr>
                                    <td   class="bg text-center">Owner name</td>
                                    <td  class="bg2">{{$company->owmer}}</td>
                            </tr>

                            <tr>
                                    <td  class="text-center bg">Manager</td>
                                    <td  class="bg2">{{$company->manger}}</td>
                            </tr>
                            <tr>
                                    <td  class="text-center bg">Manager tel</td>
                                    <td  class="bg2">{{$company->mtel}}</td>

                            </tr>

                            <tr>
                                <td scope="row" class="text-center bg">Address</td>
                                <td class="bg2" >{{$company->address}}</td>
                            </tr>
                            <tr>
                                <td scope="row" class="text-center bg">Service (Eng)</td>
                                <td class="bg2" >{{$company->service_en}}</td>
                            </tr>
                            <tr>
                                <td scope="row" class="text-center bg">Service (Ko)</td>
                                <td class="bg2" >{{$company->service_ko}}</td>
                            </tr>
                            <tr>
                                <td scope="row" class="text-center bg">Service (Vi)</td>
                                <td class="bg2" >{{$company->service_vi}}</td>
                            </tr>
                            <tr>
                                <td class="text-center bg">
                                    About us
                                </td>
                                <td  class="bg2 text-center" style="height: 5em;text-align: left;word-break: break-all">
                                    <p class="description text-align: justify;" style="padding-top: 0">
                                        @if($locale === 'ko')
                                            {{$company->about_us_ko }}
                                        @elseif($locale === 'vi' && $company->about_us_vi != null)
                                            {{$company->about_us_vi }}
                                        @elseif($locale === 'ko' && $company->about_us_ko != null)
                                            {{$company->about_us_ko }}
                                        @elseif($locale === 'en' && $company->about_us_en != null)
                                            {{$company->about_us_en }}
                                        @else
                                            {{$company->about_us_ko }}
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <div style="margin: 0 auto;text-align: center;margin-bottom: 1em">
                            <a href="{{route('client.company.index')}}" class="btn btn-primary"
                               style="background: #204181;color: #fff;width: 150px; -webkit-border-radius: 3rem; -moz-border-radius: 3rem;margin-top:3em">Back</a>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="popup-product">
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header new-title">
                            <h5 class="modal-title">Documents</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="new-item tdumbnail animation-element scroll-zoom-out in-view">
                                <table class="table">
                                    <tr>
                                        <td>Name</td>
                                        <td>Action</td>
                                    </tr>
                                    <tr>
                                        @if($company->file_excel != null)
                                            <td>{{str_replace("documents/","",$company->file_excel)}}</td>
                                            <form action="{{ route('download') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="url" value="{{ $company->file_excel }}">
                                                <td><button type="submit">Download</a></td>
                                            </form>

                                        @endif
                                    </tr>
                                    <tr>
                                        @if($company->file_pdf != null)
                                            <td>{{$company->file_pdf}}</td>
                                            <form action="{{ route('download') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="url" value="{{ $company->file_pdf }}">
                                                    <td><button type="submit">Download</a></td>
                                                </form>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <script>
            function showPopup() {
                $('#myModal').modal('show')
            }
        </script>
    </main>
@endsection

