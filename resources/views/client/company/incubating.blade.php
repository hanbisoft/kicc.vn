@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('searchCompany')}}" method="GET">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.incubating') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class=" row" style="padding-left: inherit">
                        @foreach($incubating as $value)
                            <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view"
                                 style="width: 353.333px;margin-right: auto;padding:0;margin-bottom: 1em">
                                <a href="/company/{{$value->id}}"><div class="incubat-img new-img"><img src="{{asset($value->images)}}" alt=""></div></a>
                                <div class="incubat-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/company/{{$value->id}}">{{$value->title_en}}</a></h5>
                                        <p class="description">{{str_limit($value->content_en,30)}}</p>
                                    @else
                                        <h5><a href="/company/{{$value->id}}">{{$value->title_ko}}</a></h5>
                                        <p class="description">{{str_limit($value->content_ko,30)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="float-right">  {{$incubating->links()}}</div>
                </div>
            </div>
        </section>
    </main>

@endsection