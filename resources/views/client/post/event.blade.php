@extends('master')
@section('content')

    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form  action="{{route('searchEvent')}}" method="GET" class="form-inline my-2 my-lg-0">
                        <input name="search" class="form-control mr-sm-2" type="search"
                               placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb new-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.event') }}</h3>
                    </div>
                    <div class="heading-right">
                        <form  id="form" action="{{route('client.event.index')}}" method="GET" style="display: inline">
                        <label for=""><strong>Month</strong></label>
                            <select class="select_month" name="month" id="month2">
                                <option value="">Please select</option>
                                <option  value="Jan"> January</option>
                                <option value="Feb">February</option>
                                <option value="Mar"> March</option>
                                <option value="Apr"> April</option>
                                <option value="May"> May</option>
                                <option value="Jun"> June</option>
                                <option value="Jul">July</option>
                                <option value="Aug">August</option>
                                <option value="Sep">September</option>
                                <option value="Oct">October</option>
                                <option value="Nov">November</option>
                                <option value="Dev">December</option>
                            </select>
                            <label for=""><strong>Year</strong></label>
                            <select onchange="submitForm()" class="select_month" name="year" id="year">
                                <option value="">Please select</option>
                            </select>
                        </form>
                        <a href="{{route('new.index')}}" class="btn btn-hb btn-viewall">{{ trans('messages.news') }}</a>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    <div class="row">
                        @foreach($event as $value)
                            <div class="col-md-4" style="margin-bottom: 1em">
                                <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                                    <div class="new-img" style="max-height: 233.525px"
                                         onclick="showEvent({{$value->id}})">
                                        <img src="{{asset($value->images)}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="new-box">
                                        <div class="new-time">
                                            <p>{{$value->day}}</p>
                                            <span>{{$value->month}}</span>
                                        </div>
                                        <div class="new-title ">
                                            @if($locale === 'en')
                                                <h5><a>{{$value->title_en}}</a></h5>
                                            @elseif($locale === 'vi')
                                                <h5><a>{{$value->title_vi}}</a></h5>
                                            @else
                                                <h5><a>{{$value->title_ko}}</a></h5>
                                            @endif
                                            <p><i class="fa fa-clock-o"></i>{{$value->time_start}}
                                                - {{$value->time_end}}
                                            </p>
                                            <p id="place"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>

        {{--//history--}}
        <section class="section-hb history-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.history') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="history-inner owl-carousel">
                        @foreach($histories as $value)
                            <div class="history-box thumbnail animation-element scroll-zoom-out in-view">
                                <a href="/new/{{$value->id}}/{{$value->slug}}"><img src="{{asset($value->images)}}"
                                                                                    alt=""></a>
                                <div class="history-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_en}}</a></h5>
                                        <p>{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_vi}}</a></h5>
                                        <p>{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_ko}}</a></h5>
                                        <p>{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Modal -->
    <h5 id="wait"></h5>
    <!-- Modal -->
    <h5 id="wait"></h5>

    {{--//event--}}
    <div class="popup-product">
        <div id="event-popup" class="modal fade modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <div class="new-time" data-toggle="modal" data-target="#event-popup">
                            <p id="time_start"></p>
                            <span id="month"></span>
                        </div>
                        <div class="new-title ">
                            <div class="popup-product section-hb">
                                <h5 id="event-title"></h5>
                            </div>
                            <p id="hour"></p>
                            <p id="place3"></p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img-event"></div>
                            </div>
                            <div class="new-box">
                                <p class="description" id="content-event"></p>
                            </div>
                            <div class="box-place">
                                <div class="place-item">
                                    <i class="fa fa-clock-o"></i>
                                    <p>Time: <br><span id="hour2"></span></p>
                                </div>
                                <div class="place-item">
                                    <i class="fas fa fa-map-marker"></i>
                                    <p>Place: <br><span id="place2"></span></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
    <script>


        function showEvent(t) {
            var e = document.getElementById("locale").value,
                getTitle = "";
            getUrlImage = "";
            getContent = "";
            download = "";
            get_time_start = "";
            get_time_end = "";
            get_month = "";
            getDay = "";
            $.ajax({
                url: "{{ route('api.new') }}", beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get", data: {locale: e, id: t},
                dateType: "text",
                success: function (t) {
                    console.log(t)
                    getTitle += t.title,
                        getUrlImage += "<img   class='img-fluid' src='" + t.images + "'>",
                        getContent += t.content,
                        getDay += t.day,
                        get_month += t.month,
                        get_time_start += t.time_start,
                        get_time_end += t.time_end,
                        $("#event-title").html(getTitle);
                    $("#titleEvent").html(getTitle);
                    $("#img-event").html(getUrlImage);
                    $("#content-event").html(getContent),
                    $("#time_start").html(getDay);
                    $("#place").html("<i class=\"fas fa fa-map-marker\"></i>"+t.place)
                    $("#place3").html("<i class=\"fas fa fa-map-marker\"></i>"+t.place)
                    $("#place2").html(t.place)
                    $("#month").html(get_month);
                    $("#hour").html("<i class='fa fa-clock-o'></i>" + get_time_start + " - " + get_time_end);
                    $("#hour2").html("<i class='fa fa-clock-o'></i>" + get_time_start + " - " + get_time_end);
                    $("#event-popup").modal("show")
                }
            })
        }

        //loop year
        for (i = new Date().getFullYear(); i > 2015; i--) {
            $('#year').append($('<option />').val(i).html(i));
        }
        
        function submitForm() {
            $( "#form" ).submit();
        }
    </script>
@endsection
