@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form action="{{route('searchNew')}}" method="GET" class="form-inline my-2 my-lg-0">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt=""><h3>{{ trans('messages.news') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="{{route('client.event.index')}}" class="btn btn-hb btn-viewall">{{ trans('messages.event') }}</a>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    @foreach($new as $value)
                        @if($value->cate_id === 1)
                            <div class="row media" >
                                <div class="col-4 ">
                                    <a class="new-img" href="/new/{{$value->id}}/{{$value->slug}}">
                                        <img   src="{{asset($value->images)}}"
                                               class="rounded float-left img_new"/>
                                    </a>
                                </div>
                                <div class="col-8">
                                    @if($locale == 'en')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_en}}</a></h5>
                                        <p class="description">{{$value->description_en}}</p>
                                    @elseif($locale == 'vi')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_vi}}</a></h5>
                                        <p class="description">{{$value->description_vi}}</p>
                                    @elseif($locale == 'ko')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_ko}}</a></h5>
                                        <p class="description">{{$value->description_ko}}</p>
                                    @endif

                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="float-right"> {{$new->links()}}</div>
                </div>
            </div>
        </section>

        {{--//history--}}
        <section class="section-hb history-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.history') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="history-inner owl-carousel">
                        @foreach($histories as $value)
                            <div class="history-box thumbnail animation-element scroll-zoom-out in-view">
                                <a href="/new/{{$value->id}}/{{$value->slug}}"><img src="{{asset($value->images)}}" alt=""></a>
                                <div class="history-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_en}}</a></h5>
                                        <p>{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_vi}}</a></h5>
                                        <p>{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_ko}}</a></h5>
                                        <p>{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Modal -->
    <h5 id="wait"></h5>
    <div class="popup-product">
        <div id="new" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="social-new">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId"
                                width="134" height="46" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <span id="excel"></span>
                        <span id="pdf"></span>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title"></h6>
                                    <div id="content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>
        function show(t) {
            var language = document.getElementById("locale").value,
                getTitle = "",
                getUrlImage = "",
                getContent = "",
                downloadExcel = "";
            downloadPdf = "";
            $.ajax({
                url: "{{ route('api.new') }}",
                beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get",
                data: {locale: language, id: t},
                dateType: "text", success: function (t) {
                    getTitle += t.title;
                    getUrlImage += "<img class='img-fluid' src='" + t.images + "'>";
                    downloadExcel += " <a  download href='" + t.file_excel + "' class='btn btn-hb btn-download'>Download Excel</a>";
                    downloadPdf += " <a  download href='" + t.file_pdf + "' class='btn btn-hb btn-download'>Dowload Pdf</a>";
                    getContent += t.content;
                    $("#exampleModalLongTitle").html(getTitle);
                    $("#title").html(getTitle);
                    $("#img").html(getUrlImage);
                    if(t.file_excel != null) {
                        $("#excel").html(downloadExcel);
                    }
                    if(t.file_pdf != null) {
                        $("#pdf").html(downloadPdf);
                    }
                    $("#content").html("<p class='description'>" + getContent + "</p>");
                    $("#new").modal("show");
                }
            })
        }
    </script>
@endsection
