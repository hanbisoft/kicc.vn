@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('searchB2b')}}" method="GET">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="내용을 입력해주세요"
                               aria-label="Search" value="{{$key}}">
                        <button class="btn" type="submit">조회</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.b2b_matching') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class=" row" style="padding-left: inherit">
                        @if(empty($b2b))
                            <div class="col-12 b2b_center">
                                <ul class="list-ops">
                                    <li style="margin-bottom: 1em">{{$key}} <span class="text-danger">에 대한 검색결과가 없습니다.</span></li>
                                    <li>단어의 철자가 정확한지 확인해 보세요</li>
                                    <li>한글을 영어로 혹은 영어를 한글로 입력했는지 확인해 보세요.</li>
                                    <li>검색어의 단어 수를 줄이거나, 보다 일반적인 검색어로 다시 검색해 보세요.</li>
                                    <li>두 단어 이상의 검색어인 경우, 띄어쓰기를 확인해 보세요.</li>
                                    <li>검색 옵션을 변경해서 다시 검색해 보세요.</li>
                                </ul>
                            </div>

                        @else
                            @foreach($b2b as $value)
                                <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view"
                                     style="width: 353.333px;margin-right: auto;padding:0;margin-bottom: 1em">
                                    <a href="/company/{{$value->id}}">
                                        <div class="incubat-img new-img"><img src="{{asset($value->images)}}" alt="">
                                        </div>
                                    </a>
                                    <div class="incubat-text caption">
                                        @if($locale === 'en')
                                            <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                            <p class="description">{{$value->address}}</p>
                                        @else
                                            <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                            <p class="description">{{$value->address}}</p>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection