@yield('slide')
<div id="slider-nb" class="slider-nb">
    <div class="slider-inner owl-carousel">
        <div class="slider-item">
            <img src="banner/ok4-min.jpg" alt="">
            <div class="box-text">
                <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
            </div>
        </div>
        <div class="slider-item">
            <img src="banner/ok-min.jpg" alt="">
            <div class="box-text">
                <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
            </div>
        </div>
        <div class="slider-item">
            <img src="banner/ok2-min.jpg" alt="">
            <div class="box-text">
                <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
            </div>
        </div>
        <div class="slider-item">
            <img src="banner/ok3-min.jpg" alt="">
            <div class="box-text">
                <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
            </div>
        </div>
    </div>
</div>