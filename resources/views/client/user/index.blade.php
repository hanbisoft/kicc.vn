@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb"></div>
        <div class="container">
            <div class="content">
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="row my-2">
                            <div class="col-lg-8 order-lg-2">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="" data-target="#messages" data-toggle="tab" class="nav-link">Book
                                            History</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Update
                                            Profile</a>
                                    </li>
                                </ul>
                                <div class="tab-content py-4">
                                    <div class="tab-pane active" id="profile">
                                        <table class="table">
                                            <tr>
                                                <th>Email</th>
                                                <td>{{Auth::user()->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>Phone</th>
                                                <td>{{Auth::user()->phone}}</td>
                                            </tr>
                                            <tr>
                                                <th>Name</th>
                                                <td>{{Auth::user()->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Company Name</th>
                                                <td>{{Auth::user()->company_name}}</td>
                                            </tr>
                                        </table>
                                        <!--/row-->
                                    </div>
                                    <div class="tab-pane" id="messages">
                                        @if(!empty($book))
                                            @foreach($book as $value)
                                                <table class="table">
                                                    <tr>
                                                        <th>Company name</th>
                                                        <td>{{$value->company_name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Room</th>
                                                        <td> {{$value->room_id}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Time Start</th>
                                                        <td> {{$value->start_date}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Time End</th>
                                                        <td> {{$value->end_date}}</td>
                                                    </tr>
                                                    @if($value->start_date < $mytime)
                                                        <tr>
                                                            <th>
                                                                Status
                                                            </th>
                                                            <td>
                                                                <span class="text-success">Completed</span>
                                                            </td>
                                                        </tr>
                                                    @elseif($value->start_date > $mytime)
                                                        <tr>
                                                            <td colspan="4" style="margin: 0 auto;text-align: center">
                                                                <a href="http://kicc.vn//booking-cancer/{{$value->id}}"
                                                                   class="btn btn-danger">Cancel
                                                                    reservation</a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            @endforeach
                                            {{$book->links()}}
                                        @endif
                                    </div>

                                    <div class="tab-pane" id="edit">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @if(session()->has('message'))
                                                    <div class="alert alert-success">
                                                        {{ session()->get('message') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <form role="form" action="{{route('user.update')}}" method="GET">
                                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Company
                                                    name</label>
                                                <div class="col-lg-9">
                                                    <input name="name" class="form-control" type="text"
                                                           value="{{Auth::user()->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Email</label>
                                                <div class="col-lg-9">
                                                    <input name="email" class="form-control" type="text"
                                                           value="{{Auth::user()->email}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Phone</label>
                                                <div class="col-lg-9">
                                                    <input name="phone" class="form-control" type="text"
                                                           value="{{Auth::user()->phone}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                                <div class="col-lg-9">
                                                    <input type="reset" class="btn btn-secondary" value="Cancel">
                                                    <input type="submit" class="btn btn-primary" value="Save Changes">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 order-lg-1 text-center">
                                @if(empty($images))
                                    <img src="//placehold.it/150"
                                         class="mx-auto img-fluid img-circle d-block" alt="avatar">
                                    <br>
                                    <a href="#" onclick="editImage()">Edit Avatar</a>
                                @else
                                    <img src="{{asset($images)}}"
                                         class="mx-auto img-fluid img-circle d-block" alt="avatar">
                                    <br>
                                    <a href="#" onclick="editImage()">Edit Avatar</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <div class="modal" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Avatar </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('user-upload')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input required type="file" class="form-control" name="img" id="img">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function editImage() {
            $('#myModal').modal('show')
        }
    </script>
@endsection
