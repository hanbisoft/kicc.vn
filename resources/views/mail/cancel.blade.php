<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <style>
        *{font-family:'Open Sans',sans-serif}.table{width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent;margin-left:30px;margin-right:30px;border-spacing:2px;border-collapse:collapse}tr{display:table-row;vertical-align:inherit;border-color:inherit}.table-bordered td,.table-bordered th{border:1px solid #dee2e6}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}td,th{display:table-cell;vertical-align:inherit}.btn_button{text-decoration:none;background:#c82333;color:#fff!important;margin-top:1em;display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}.description{font-size:15px!important;line-height:25px!important;word-wrap:break-word;padding-right:36px;text-align:justify}
    p {
        text-align: center;
    font-size: 14px;
    color: red;
    }
    </style>
</head>
<body>
<div id="page-container"
     style="margin:0; padding:0; border:none; overflow:auto;background-color:#9e9e9e;background-image:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1IiBoZWlnaHQ9IjUiPgo8cmVjdCB3aWR0aD0iNSIgaGVpZ2h0PSI1IiBmaWxsPSIjOWU5ZTllIj48L3JlY3Q+CjxwYXRoIGQ9Ik0wIDVMNSAwWk02IDRMNCA2Wk0tMSAxTDEgLTFaIiBzdHJva2U9IiM4ODgiIHN0cm9rZS13aWR0aD0iMSI+PC9wYXRoPgo8L3N2Zz4=');">
    <div id="pf1" class="pf w0 h0" data-page-no="1"
         style="position:relative;background-color:white;overflow:hidden;border-width:0;margin-top:13px;margin-bottom:13px;margin-right:auto;margin-left:auto;box-shadow:1px 1px 3px 1px #333;border-collapse:separate;height:890px;width:700px;">
        <div class="pc pc1 w0 h0"
             style="margin:0; padding:0; border:none; overflow:hidden;display:block; height:890px;width:700px; background-image:url(http://ep.hanbisoft.com/pdf/bg-mail.jpg)">
            <h2 style="margin-top: 148px;margin-left: 226px;color: #ffffff">CANCEL BOOKING</h2>
            <br>
            <p>Your meeting room has been canceled by Admin, please contact kicc, if you have any questions.</p>
            <table class="table">
                <tr>
                    <th scope="row" style="text-align: center;width: 200px">
                        ID
                    </th>
                    <td style="text-align: center">
                        {{$room}}
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="text-align: center;width: 200px">
                        Time start
                    </th>
                    <td style="text-align: center">
                        {{$time_start}}
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="text-align: center;width: 200px">
                        Time End
                    </th>
                    <td style="text-align: center">
                        {{$time_end}}
                    </td>
                </tr>
            </table>
        </div>
        <div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'
             style="display:none;">
        </div>
    </div>
</div>
</body>
</html>
