<!DOCTYPE html>
<!-- saved from url=(0016)http://localhost -->
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KICC -Korea IT Cooperation Center in Hanoi</title>
    <meta name="keywords" content="kicc.vn,Korea IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="KICC -Korea IT Cooperation Center in Hanoi"/>
    <meta property="og:description"
          content="About usKorea IT Cooperation Center (KICC) in Hanoi Established in November 2017, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading"/>
    <meta property="og:url" content="http://kicc.vn/"/>
    <meta property="og:site_name" content="KICC -Korea IT Cooperation Center in Hanoi"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:description"
          content="About usKorea IT Cooperation Center (KICC) in Hanoi Established in November 2017, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading"/>
    <!--Fonts-->

    <link rel="icon" href="{{asset('images/kicc.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Styles-->
    <link href="{{asset('dist/vendor.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('dist/theme.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('css/style_khanh.css')}}" rel="stylesheet">
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <!--Icons fonts-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('style')
    <script src="{{asset('dist/vendor-bootstrap.js')}}" type="text/javascript" defer></script>
    <script src="{{asset('dist/custom.js')}}" type="text/javascript" defer></script>

    <script src="{{asset('dist/custom.js')}}" type="text/javascript" defer></script>
    @if (\Route::current()->getName() != 'client.book.index')
        @if (\Route::current()->getName() != 'client.book-lager')
            @if (\Route::current()->getName() != 'client.book-small')
                @if (\Route::current()->getName() != 'client.book-open')
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                @endif
            @endif
        @endif
    @endif


</head>
<body>
<header class="header-hb">
    <nav class="navbar navbar-expand-lg navbar-light nav-hb" style="height: 80px;">
        <div class="container">
            <a class="navbar-brand logo" href="/">
                <img src="{{asset('images/logo.png')}}" alt="" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" style="margin-left: auto !important;margin-right: 0 !important;" id="menu">
                    {{--  <li class="nav-item active">
                        <a class="nav-link" href="{{route('about.index')}}">{{ trans('messages.about') }} <span
                                    class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('new.index')}}">{{ trans('messages.news') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{route('client.company.index')}}">{{ trans('messages.company_infomation') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{route('client.showroom.index')}}">{{ trans('messages.showroom') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('support')}}">{{ trans('messages.support') }}</a>
                    </li>
                    @if (Auth::check())
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('client.share-room')}}">{{trans('messages.share_room')}}</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('client.share-room')}}">{{trans('messages.share_room')}}</a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('asean')}}">{{trans('messages.asean')}}</a>
                    </li>  --}}
                </ul>
            </div>
            <div class="nav-right" style="margin-left: 1em;">
                @guest
                    <div class="dr dropdown">
                        <a data-toggle="dropdown" href="{{route('login')}}"
                           class="btn btn_button">{{ trans('messages.login') }}</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('login') }}">{{trans('messages.login')}}</a>
                            <a class="dropdown-item" href="{{ route('register') }}">{{trans('messages.register')}}</a>
                        </div>
                    </div>

                @endguest

                <ul class="nav-current">
                    @auth
                        <li>
                            <ul class="navbar-nav mr-auto" style="margin-right: 0">
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <i class="fa fa-user-circle" aria-hidden="true"></i> <span class="caret"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                        <a class="dropdown-item" href="{{route('user.profile')}}">User Profile</a>
                                        @if (Auth::user()->isAdmin === 1)
                                        <a class="dropdown-item" href="/admin">Admin</a>
                                        @endif
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>

                            </ul>
                        </li>
                    @endauth
                    <li><a href="{!! route('user.change-language', ['en']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_06.jpg')}}" alt=""></a></li>
                    <li><a href="{!! route('user.change-language', ['vi']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_08.jpg')}}" alt=""></a></li>
                    <li><a href="{!! route('user.change-language', ['ko']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_10.jpg')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main class="main-hb">
    @yield('content')
</main>
<img id="button" src="{{asset('images/top.png')}}" alt="">
<footer class="footer-hb section-hb animation-element slide-left footer_bt" style="padding-top: 0">
    <div class="footer-top heading">
        <div class="container">
            <div class="row">
                <div class="col-md-8 footer-left">
                    <div class="heading">
                        <div class="heading-left text-center">
                            <img src="{{asset('images/iconchuan2.jpg')}}" alt="">
                            <h3 class="text-footer">Contact us</h3>
                        </div>
                    </div>
                    <h5 class="text-footer">KOREA IT COOPERATION CENTER IN HANOI </h5>
                    <ul class="footer-info">
                        <li><img src="{{asset('images/icon_03.jpg')}}" alt=""> <span>Address: 25th Floor, Keangnam Land Tower, Nam Tu Liem, Hanoi </span>
                        </li>
                        <li><img src="{{asset('images/icon_06.jpg')}}" alt="">
                            <span>Hotline: (84)24 7300 0672 (Korean) </span></li>
                        <li><img src="{{asset('images/icon_09.jpg')}}" alt=""> <span>Email: kicchanoi@nipa.kr</span>
                        </li>
                        <li><img src="{{asset('images/icon_12.jpg')}}" alt=""> <span>Website: <a href=""
                                                                                                 class="text-web">http://kicc.vn/</a> </span>
                        </li>
                        <li><img src="{{asset('images/icon_12.jpg')}}" alt=""> <span>Connect us:
                               <a href="https://band.us/home"><img src="{{asset('images/band.jpg')}}"
                                                                   style="width: 30px" alt="" class="social-band"></a>
                                <a href="https://www.facebook.com/KICC.vn/"><img src="{{asset('images/fb.jpg')}}" alt=""
                                                                                 style="width: 30px"
                                                                                 class="social-face"></a></span></li>
                    </ul>
                </div>
                <div class="col-md-4 footer-right">
                    <div class="link-social">
                        <div class="footer-face">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F&tabs=timeline&width=275&height=110&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"
                                    width="275" height="130" style="border:none;overflow:hidden" scrolling="no"
                                    frameborder="0" allowTransparency="true" allow="encrypted-media">
                            </iframe>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <iframe width="100%" height="400" id="gmap_canvas"
                src="https://maps.google.com/maps?q=kangnam%20tower%20h%C3%A0%20n%E1%BB%99i%20vi%E1%BB%87t%20nam&t=&z=13&ie=UTF8&iwloc=&output=embed"
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="text-center">© 2019 KICC.vn Designer by <a href="http://hanbisoft.com">Hanbisoft</a></p>
        </div>
    </div>

</footer>


<script>
    var btn = $("#button");
    $(window).scroll(function () {
        $(window).scrollTop() > 300 ? btn.addClass("show") : btn.removeClass("show")
    }), btn.on("click", function (o) {
        o.preventDefault(), $("html, body").animate({scrollTop: 0}, "300")
    });

    this.showMenu();
    function showMenu()
    {
        var htmlResult = ""
        $.ajax({
            url: "{{ route('api.menu') }}",
            type: "get",
            dateType: "text",
            success: function (result) {
                Object.keys(result).forEach(function(key) {
                    htmlResult += " <li class='nav-item'><a class='nav-link' href='"+result[key].url+"'>"+result[key].name+"</a></li>";
                  })
                $("#menu").append(htmlResult);
            }
        });
    }
</script>
</body>
</html>
