@extends('mobile.mobile-master')
@section('style')
    {{--<link rel="stylesheet" href="{{asset('css/admin.css')}}">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{asset('calender/fullcalendar.min.css')}}' rel='stylesheet'/>
    <link href='{{asset('calender/fullcalendar.print.min.css')}}' rel='stylesheet' media='print'/>
    <script src='{{asset('calender/lib/moment.min.js')}}'></script>
    <script src='{{asset('calender/lib/jquery.min.js')}}'></script>
    <script src='{{asset('calender/fullcalendar.min.js')}}'></script>
    <style>
        .fc-time{color:#fff}#calendar{margin:1em auto 0;width:inherit;background-color:#FFF;border-radius:6px;-webkit-box-shadow:0 0 21px 2px rgba(0,0,0,.18);-moz-box-shadow:0 0 21px 2px rgba(0,0,0,.18);box-shadow:0 0 21px 2px rgba(0,0,0,.18)}.fc-time>span,.fc-title{color:#fff;font-weight:700}
        .book{height:auto;background:#204181;color:#fff;width:100px;-webkit-border-radius:3rem;-moz-border-radius:3rem;border-radius:3rem;margin-top:3em}.center{margin:0 auto}.fc-center{font-size:30px;font-weight:700}
    </style>
@endsection
@section('content')
    <main>
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                    </div>
                </div>
                <div id='calendar'>
                    <button type="button" class="btn btn-sm btn-primary">Large meeting room</button>
                    <button type="button" class="btn btn-sm btn-success">Small Meeting room</button>
                    <button type="button" class="btn btn-sm btn-danger">Open Co-Working Space</button>
                </div>
            </div>
        </section>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{route('client.book.new')}}" method="GET" id="book">
                        <input type="hidden" name="user_id" id="user_id" value=" {{Auth::user()->id}}">
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: #1b3f82;text-align: center;font-weight: bold" id="">
                                MEETING ROOM RESERVATION</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <h5 id="wait"></h5>
                            <h5 class="text-danger" id="mess_error"></h5>
                            <div class="form-group">
                                <label for=""><strong class="text-danger">*</strong> Company name</label>
                                <input required type="text" class="form-control" value="{{Auth::user()->name}}"
                                       name="company_name" id="company_name"/>
                            </div>
                            <div class="form-group">
                                <label for="">Room</label>
                                <select name="room_id" id="room_id" class="form-control" id="room_id">
                                    <option value="1">Large meeting room</option>
                                    <option value="2">Small Meeting room</option>
                                    <option value="3">Open Co-Working Space</option>
                                </select>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for=""> <strong class="text-danger">*</strong> Start Date </label>
                                    <input required type="date" class="form-control" name="start_date" id="start_date"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> Time Start</label>
                                    <select name="time_start" class="form-control" id="time_start">
                                        <option value="8:00:00">8h am</option>
                                        <option value="9:00:00">9h am</option>
                                        <option value="10:00:00">10h am</option>
                                        <option value="11:00:00">11h am</option>
                                        <option value="12:00:00">12h am</option>
                                        <option value="13:00:00">13h pm</option>
                                        <option value="14:00:00">14h pm</option>
                                        <option value="15:00:00">15h pm</option>
                                        <option value="16:00:00">16h pm</option>
                                        <option value="17:00:00">17h pm</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> Time End</label>
                                    <select name="time_end" class="form-control" id="time_end">
                                        <option value="9:00:00">9h am</option>
                                        <option value="10:00:00">10h am</option>
                                        <option value="11:00:00">11h am</option>
                                        <option value="12:00:00">12h am</option>
                                        <option value="13:00:00">13h pm</option>
                                        <option value="14:00:00">14h pm</option>
                                        <option value="15:00:00">15h pm</option>
                                        <option value="16:00:00">16h pm</option>
                                        <option value="17:00:00">17h pm</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">Total number of people</label>
                                    <input  type="number" class="form-control" id="qty" value="2" name="qty"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Note</label>
                                    <textarea name="note" style="max-height: 54px" class="form-control" id="note" cols="5"
                                              rows="5"></textarea>
                                </div>
                                <div class="text-center center"> <button onclick="show()" type="button" class="book btn btn-primary">Book</button></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- Modal -->
        <script>
            var dataResult = [];
            $.ajax({
                url: "{{ route('client.book.show') }}",
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                type: "get",
                dateType: "json",
                success: function (result) {
                    dataResult = result;
                    $('#calendar').fullCalendar({
                        defaultView:'month',
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listWeek'
                        },
                        timeFormat:'H:mm',
                        defaultDate: new Date(),
                        navLinks: true, // can click day/week names to navigate views
                        selectable: true,
                        selectHelper: true,
                        select: function (start, end) {
                            var date_default= start;
                            console.log(date_default.format())
                            $('#start_date').val(date_default.format());
                            $('#myModal').modal('show')
                        },
                        editable: true,
                        eventLimit: true, // allow "more" link when too many events
                        events: dataResult,
                        eventColor: '#ffffff',
                        loading: function (bool) {
                            $('#loading').toggle(bool);
                        }
                    });
                }
            });
            function show() {
                $body = $("body");
                var user_id = document.getElementById('user_id').value;
                var company_name = document.getElementById('company_name').value;
                var room_id = document.getElementById('room_id').value;
                var start_date = document.getElementById('start_date').value;
                var time_start = document.getElementById('time_start').value;
                var time_end = document.getElementById('time_end').value;
                var note = document.getElementById('note').value;
                var qty = document.getElementById('qty').value;
                $.ajax({
                    url: "{{ route('client.book.new') }}",
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    type: "get",
                    data: {
                        'user_id': user_id,
                        'company_name': company_name,
                        'room_id': room_id,
                        'start_date': start_date,
                        'time_start': time_start,
                        'time_end': time_end,
                        'qty': qty,
                        'note': note
                    },
                    dateType: "text",
                    success: function (result) {
                        console.log(result)
                        if(result.code === 200) {
                            location.href='{{route('book.confirm')}}'
                        } else  {
                            console.log(result)
                            $('#mess_error').html("").html(result.mess)
                        }

                    }
                });
            }
        </script>
    </main>
@endsection