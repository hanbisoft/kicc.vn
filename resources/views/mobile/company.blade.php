@extends('mobile.mobile-master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="images/icon-showroom.jpg" alt=""><h3>{{ trans('messages.incubating') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    @foreach($company as $value)
                        @if($value->cate_id === 2)
                        <div class="row media">
                            <div class="col-5 new-img">
                                <a href="/mobile/company/{{$value->id}}"><img  src="{{asset($value->images)}}"
                                                                                      class="img-mobile" alt=""/></a>
                            </div>
                            <div class="col-7">
                                @if($locale == 'en')
                                    <h5><a  class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                    <p class="description">{{$value->address}}</p>
                                @elseif($locale == 'vi')
                                    <h5><a class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                    <p class="description">{{$value->address}}glkll','
                                @elseif($locale == 'ko')
                                    <h5><a class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                    <p class="description">{{$value->address}}</p>
                                @endif
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="images/icon-showroom.jpg" alt=""><h3>{{ trans('messages.b2b_matching') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    @foreach($company as $value)
                        @if($value->cate_id === 3)
                            <div class="row media">
                                <div class="col-5 new-img">
                                    <a href="/mobile/company/{{$value->id}}"><img  src="{{asset($value->images)}}"
                                                                                   class="img-mobile" alt=""/></a>
                                </div>
                                <div class="col-7">
                                    @if($locale == 'en')
                                        <h5><a  class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                        <p class="description">{{$value->address}}</p>
                                    @elseif($locale == 'vi')
                                        <h5><a class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                        <p class="description">{{$value->address}}glkll','
                                    @elseif($locale == 'ko')
                                        <h5><a class="text-mobile" href="/mobile/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                        <p class="description">{{$value->address}}</p>
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="float-right"> {{$company->links()}}</div>
                </div>
            </div>
        </section>
    </main>
@endsection