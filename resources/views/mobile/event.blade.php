@extends('mobile.mobile-master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">

        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="images/icon-showroom.jpg" alt=""><h3>{{ trans('messages.event') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="/mobile/new" class="btn btn-hb btn-viewall">{{ trans('messages.news') }}</a>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    @foreach($event as $value)
                        @if($value->cate_id === 2)
                        <div class="row media" onclick="showEvent({{$value->id}});">
                            <div class="col-5 new-img">
                                <img  src="{{asset($value->images)}}"
                                      class="img-mobile"/>
                            </div>
                            <div class="col-7">
                                @if($locale == 'en')
                                    <h5><a  class="text-mobile" onclick="showEvent({{$value->id}});">{{$value->title_en}}</a></h5>
                                    <p class="description">{{$value->description_en}}</p>
                                @elseif($locale == 'vi')
                                    <h5><a class="text-mobile" onclick="showEvent({{$value->id}});">{{$value->title_vi}}</a></h5>
                                    <p class="description">{{$value->description_vi}}glkll','
                                @elseif($locale == 'ko')
                                    <h5><a class="text-mobile" onclick="showEvent({{$value->id}});">{{$value->title_ko}}</a></h5>
                                    <p class="description">{{$value->description_ko}}</p>
                                @endif

                            </div>
                        </div>
                        @endif
                    @endforeach
                    <div class="float-right"> {{$event->links()}}</div>
                </div>
            </div>
        </section>

        {{--//history--}}
        <section class="section-hb history-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-history.jpg')}}" alt="">
                        <h3>{{ trans('messages.history') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="history-inner owl-carousel">
                        @foreach($histories as $value)
                            <div class="history-box thumbnail animation-element scroll-zoom-out in-view">
                                <div class="new-img  " onclick="show({{$value->id}})">
                                    <img src="{{asset($value->images)}}" alt="">
                                </div>
                                <div class="history-text caption">
                                    @if($locale === 'en')
                                        <h5><a onclick="show({{$value->id}})">{{$value->title_en}}</a></h5>
                                        <p>{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a onclick="show({{$value->id}})">{{$value->title_vi}}</a></h5>
                                        <p>{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a onclick="show({{$value->id}})">{{$value->title_ko}}</a></h5>
                                        <p>{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Modal -->
    <h5 id="wait"></h5>
    <!-- Modal -->
    <h5 id="wait"></h5>
    {{--//history--}}
    <div class="popup-product">
        <div id="new"  class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="social-new">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="134" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <a href="" class="btn btn-hb btn-download">download</a>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img"><div id="img"></div></div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title"> </h6>
                                    <div id="content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    {{--//event--}}
    <div class="popup-product">
        <div id="event-popup" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <div class="new-time" data-toggle="modal" data-target="#event-popup">
                            <p id="time_start"></p>
                            <span id="month"></span>
                        </div>
                        <div class="new-title ">
                            <div class="popup-product section-hb">
                                <h5 id="event-title"></h5>
                            </div>
                            <p id="hour"></p>
                            <p><i class="fas fa fa-map-marker"></i>Keangnam Hanoi</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img-event"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title-event"></h6>
                                    <div class="description" id="content-event"></div>
                                </div>
                            </div>
                            <div class="box-place">
                                <div class="place-item">
                                    <i class="fa fa-clock-o"></i>
                                    <p>Time: <br><span id="hour2"></span></p>
                                </div>
                                <div class="place-item">
                                    <i class="fas fa fa-map-marker"></i>
                                    <p>Place: <br><span>Keangnam Hanoi</span></p>
                                </div>
                                <div class="place-item">
                                    <div class="social-popup">
                                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </div>
                                    </a>
                                    <a href="">share</a>
                                </div>
                                <div class="place-item">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <a href="">Add to your calendar </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
    <script>
        function show($id) {
            var locale = document.getElementById('locale').value;
            var title = '';
            var images = '';
            var content = '';
            var socical = '';
            $.ajax({
                url: "{{ route('api.new') }}",
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                type: "get",
                data: {
                    'locale': locale,
                    'id': $id
                },
                dateType: "text",
                success: function (result) {
                    console.log(result)
                    title += result.title;
                    images += "<img   class='img-fluid' src='../" + result.images + "'>";
                    content += result.content;
                    $("#exampleModalLongTitle").html(title);
                    $("#title").html(title);
                    $("#img").html(images);
                    $("#content").html("<p class='description'>"+content+"</p>");
                    $('#new').modal('show')
                }
            });
        }

        function showEvent(t) {
            var e = document.getElementById("locale").value, l = "", o = "", n = "", a = "", i = "", c = "", m = "";
            $.ajax({
                url: "{{ route('api.new') }}", beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get", data: {locale: e, id: t}, dateType: "text", success: function (t) {
                    console.log(t), l += t.title, o += "<img   class='img-fluid' src='../" + t.images + "'>", n += t.content, i += t.day, a += t.month, c += t.time_start, m += t.time_end, $("#event-title").html(l), $("#titleEvent").html(l), $("#img-event").html(o), $("#content-event").html(n), $("#time_start").html(i), $("#month").html(a), $("#hour").html("<i class='fa fa-clock-o'></i>" + c + " - " + m), $("#hour2").html("<i class='fa fa-clock-o'></i>" + c + " - " + m), $("#event-popup").modal("show")
                }
            })
        }
    </script>
@endsection