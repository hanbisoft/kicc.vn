@extends('mobile.mobile-master')
@section('content')
    <div id="slider-nb" class="slider-nb" style="margin-top: 1em !important;">
        <div class="slider-inner owl-carousel">
            <div class="slider-item">
                <img src="{{asset('banner/ok4-min.jpg')}}" alt="">
                <div class="box-text">
                    <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                    <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                </div>
            </div>
            <div class="slider-item">
                <img src="{{asset('banner/ok-min.jpg')}}" alt="">
                <div class="box-text">
                    <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                    <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                </div>
            </div>
            <div class="slider-item">
                <img src="{{asset('banner/ok2-min.jpg')}}" alt="">
                <div class="box-text">
                    <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                    <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                </div>
            </div>
            <div class="slider-item">
                <img src="{{asset('banner/ok3-min.jpg')}}" alt="">
                <div class="box-text">
                    <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                    <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="search-hb section-hb animation-element slide-left">
        <div class="container">
            <div class="search-inner">
                <form class="form-inline my-2 my-lg-0" action="{{route('search')}}" method="GET"
                      style="display: inline-flex;margin-right: auto">
                    <input name="search" class="form-control mr-sm-2" type="search" placeholder="내용을 입력해주세요"
                           aria-label="Search">
                    <input type="hidden" name="locale" id="locale" value="{{$locale}}">
                    <button class="btn" type="submit">조회</button>
                </form>
            </div>
        </div>
    </div>
    <h5 id="wait"></h5>
    <section class="section-hb new-hb animation-element slide-left">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <h3>{{ trans('messages.new_event') }}</h3>
                </div>
                <div class="heading-right">
                    <a href="{{route('mobile.event')}}" class="btn btn-hb btn-viewall">{{ trans('messages.event') }}</a>
                    <a href="{{route('mobile.new')}}" class="btn btn-hb btn-viewall">{{ trans('messages.news') }}</a>
                </div>
            </div>
            <div class="content" style="margin-top: 1em !important;">
                @foreach($event as $value)
                <div class="event_wrap">
                    <div class="col-5">
                       <div class="new-img" onclick="showEvent({{$value->id}})"> <img class="img-mobile" src="{{asset($value->images)}}" alt=""></div>
                    </div>
                    <div class="col-7">
                        <h5><a href="">{{$value->title_en}}</a></h5>
                        <p class="description3"><i class="fa fa-clock-o"></i> {{$value->time_start}}- {{$value->time_end}}</p>
                        <p class="description3"><i class="fas fa fa-map-marker"></i> Keangnam Hanoi</p>
                    </div>
                </div>
                 @endforeach
            </div>
        </div>
    </section>
    <section class="section-hb incubat-hb animation-element slide-left">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <h3>{{ trans('messages.incubating') }}</h3>
                </div>
                <div class="heading-right">
                    <a href="{{route('client.company.index')}}"
                       class="btn btn-hb btn-viewall">{{ trans('messages.view_all') }}</a>
                </div>
            </div>
            <div class="content" style="margin-top: 1em !important;">
                @foreach($incubating as $value)
                    <div class="event_wrap">
                        <div class="col-5">
                            <div class="new-img"> <img class="img-mobile" src="{{asset($value->images)}}" alt=""></div>
                        </div>
                        <div class="col-7">
                            @if($locale === 'en')
                                <h5 class="title"><a href="/mobile/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                            @elseif($locale === 'vi')
                                <h5 class="title"><a href="/mobile/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                            @else
                                <h5 class="title"><a href="/mobile/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                            @endif
                            <p class="description">{{str_limit($value->address,30)}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section-hb showroom-hb animation-element slide-left showhome " style="padding-bottom: 1em">
        <div class="container">
            <div class="heading">
                <div class="heading-left">

                    <h3 class="text-footer">{{ trans('messages.showroom') }}</h3>
                </div>
                <div class="heading-right">
                    <a href="{{route('mobile.showroom')}}" class="btn btn-hb btn-viewall btn-showroom">{{ trans('messages.view_all') }}</a>
                </div>
            </div>
            <div class="content" style="margin-top: 1em !important;">
                @foreach($showrom as $value)
                    <div class="event_wrap">
                        <div class="col-5">
                            <a href="/mobile/showroom/{{$value->id}}" class="new-img"> <img class="img-mobile" src="{{asset($value->images)}}" alt=""></a>
                        </div>
                        <div class="col-7">
                            @if($locale === 'en')
                                <h5><a class="text-showroom" href="/mobile/showroom/{{$value->id}}">{{$value->title_en}}</a></h5>
                                <p class="text-footer">{{str_limit($value->description_en,100)}}</p>
                            @elseif($locale === 'vi')
                                <h5><a class="text-showroom" href="/mobile/showroom/{{$value->id}}">{{$value->title_vi}}</a></h5>
                                <p class="text-footer">{{str_limit($value->description_vi,100)}}</p>
                            @else
                                <h5><a class="text-showroom" href="/mobile/showroom/{{$value->id}}">{{$value->title_ko}}</a></h5>
                                <p class="text-footer">{{str_limit($value->description_ko,100)}}</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section-hb match-hb animation-element slide-left">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <h3>{{ trans('messages.b2b_matching') }}</h3>
                </div>
                <div class="heading-right">
                    <a href="{{route('client.company.index')}}"
                       class="btn btn-hb btn-viewall">{{ trans('messages.view_all') }}</a>
                </div>
            </div>
            <div class="content" style="margin-top: 1em !important;">
                @foreach($b2b_matching as $value)
                    <div class="event_wrap">
                        <div class="col-5">
                            <a href="/mobile/company/{{$value->id}}" class="new-img"> <img class="img-mobile" src="{{asset($value->images)}}" alt=""></a>
                        </div>
                        <div class="col-7">
                            @if($locale === 'en')
                                <h5><a href="/mobile/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                <p>{{str_limit($value->address,100)}}</p>
                            @elseif($locale === 'vi')
                                <h5><a href="/mobile/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                <p>{{str_limit($value->address,100)}}</p>
                            @else
                                <h5><a href="/mobile/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                <p>{{str_limit($value->address,100)}}</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section-hb history-hb animation-element slide-left">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <h3>{{ trans('messages.history') }}</h3>
                </div>
            </div>
            <div class="content" style="margin-top: 1em !important;">
                @foreach($history as $value)
                    <div class="event_wrap">
                        <div class="col-5">
                            <div class="new-img  " onclick="show({{$value->id}})">
                                <img src="{{asset($value->images)}}" alt="">
                            </div>
                        </div>
                        <div class="col-7">
                            @if($locale === 'en')
                                <h5><a onclick="show({{$value->id}})">{{$value->title_en}}</a></h5>
                                <p>{{str_limit($value->description_en,100)}}</p>
                            @elseif($locale === 'vi')
                                <h5><a onclick="show({{$value->id}})">{{$value->title_vi}}</a></h5>
                                <p>{{str_limit($value->description_vi,100)}}</p>
                            @else
                                <h5><a onclick="show({{$value->id}})">{{$value->title_ko}}</a></h5>
                                <p>{{str_limit($value->description_ko,100)}}</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section-hb share-hb animation-element slide-left">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <img src="{{asset('images/icon-share.jpg')}}" alt="">
                    <h3>{{ trans('messages.share_room') }}</h3>
                </div>
            </div>
            <div class="content">
                <div class="share-inner owl-carousel">
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img"><img src="{{asset('images/lage.jpg')}}" alt=""></div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Large meeting room</a></h5>
                            <a href="/book/1" class="btn btn_book">Book</a>
                        </div>
                    </div>
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img">
                            <img src="{{asset('images/smal-office.jpg')}}" alt="">
                        </div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Small Meeting room </a></h5>
                            <a href="/book/2" class="btn btn_book">Book</a>
                        </div>
                    </div>
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img">
                            <img src="{{asset('images/co.jpg')}}" alt="">
                        </div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Open Co-Working Space</a></h5>
                            <a href="/book/3" class="btn btn_book">Book</a>
                        </div>
                    </div>
                </div>
                <div class="brand-hb owl-carousel">
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo1.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo2.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo3.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo4.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo2.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo3.jpg')}}" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="{{asset('images/sharelogo4.jpg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--//modal history--}}
    <div class="popup-product">
        <div id="new" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="social-new">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId"
                                width="134" height="46" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <div id="link"></div>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title"></h6>
                                    <div id="content"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    {{--//modal event--}}
    <div class="popup-product">
        <div id="event-popup" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <div class="new-time" data-toggle="modal" data-target="#event-popup">
                            <p id="time_start"></p>
                            <span id="month"></span>
                        </div>
                        <div class="new-title ">
                            <div class="popup-product section-hb">
                                <h5 id="event-title"></h5>
                            </div>
                            <p id="hour"></p>
                            <p><i class="fas fa fa-map-marker"></i>Keangnam Hanoi</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img-event"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title-event"></h6>
                                    <div class="description" id="content-event"></div>
                                </div>
                            </div>
                            <div class="box-place">
                                <div class="place-item">
                                    <i class="fa fa-clock-o"></i>
                                    <p>Time: <br><span id="hour2"></span></p>
                                </div>
                                <div class="place-item">
                                    <i class="fas fa fa-map-marker"></i>
                                    <p>Place: <br><span>Keangnam Hanoi</span></p>
                                </div>
                                <div class="place-item">
                                    <div class="social-popup">
                                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </div>
                                    </a>
                                    <a href="">share</a>
                                </div>
                                <div class="place-item">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <a href="">Add to your calendar </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
    <script>
        function show(t) {
            console.log(t);
            var e = document.getElementById("locale").value, l = "", o = "", n = "", a = "";
            $.ajax({
                url: "{{ route('api.new') }}", beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get", data: {locale: e, id: t}, dateType: "text", success: function (t) {
                    console.log(t), l += t.title, o += "<img   class='img-fluid' src='" + t.images + "'>", a += " <a download href='" + t.link + "' class='btn btn-hb btn-download'>download</a>", n += t.content, $("#exampleModalLongTitle").html(l), $("#title").html(l), $("#img").html(o), $("#link").html(a), $("#content").html("<p class='description'>" + n + "</p>"), $("#new").modal("show")
                }
            })
        }

        function showEvent(t) {
            var e = document.getElementById("locale").value, l = "", o = "", n = "", a = "", i = "", c = "", m = "";
            $.ajax({
                url: "{{ route('api.new') }}", beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get", data: {locale: e, id: t}, dateType: "text", success: function (t) {
                    console.log(t), l += t.title, o += "<img   class='img-fluid' src='" + t.images + "'>", n += t.content, i += t.day, a += t.month, c += t.time_start, m += t.time_end, $("#event-title").html(l), $("#titleEvent").html(l), $("#img-event").html(o), $("#content-event").html(n), $("#time_start").html(i), $("#month").html(a), $("#hour").html("<i class='fa fa-clock-o'></i>" + c + " - " + m), $("#hour2").html("<i class='fa fa-clock-o'></i>" + c + " - " + m), $("#event-popup").modal("show")
                }
            })
        }
    </script>
@endsection