@extends('mobile.mobile-master')
@section('content')
    <main class="main-hb">
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('search')}}" method="GET"
                          style="display: inline-flex;margin-right: auto">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="내용을 입력해주세요"
                               aria-label="Search">
                        <input type="hidden" name="locale" id="locale" value="{{$locale}}">
                        <button class="btn" type="submit">조회</button>
                    </form>
                    <a href="https://band.us/home"><img src="{{asset('images/band.jpg')}}" alt=""
                                                        class="social-band"></a>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="content">
                    <div class="row">
                        <div class="col-6 center">
                            <img class="img-mobile" src="{{asset($showroom->images)}}" alt=""/>
                        </div>
                        <div class="col-6">
                            @if($locale == 'en')
                                <h5 class=" show-title">{{$showroom->title_en}}</h5>
                                <p class="description">{{$showroom->description_en}}</p>
                            @elseif($locale == 'vi')
                                <h5 class=" show-title">{{$showroom->title_vi}}</h5>
                                <p class="description">{{$showroom->description_vi}}</p>

                            @elseif($locale == 'ko')
                                <h5 class=" show-title">{{$showroom->title_ko}}</h5>
                                <p class="description">{{$showroom->description_ko}}</p>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <h5 class="title">Contact</h5>
                            <p class="description"> <strong>TEL : 098989546</strong> </p>
                            <p class="description"><strong> E-mail: somgf@fd.com </strong></p>
                            <p class="description"><strong>ADDRESS: Lorem ipsum dolor sit amet</strong></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <h5 class="title">Item Details</h5>

                            @if($locale == 'en')
                                <p class="description">{{$showroom->content_en}}</p>
                            @elseif($locale == 'vi')
                                <p class="description">{{$showroom->content_vi}}</p>
                            @elseif($locale == 'ko')
                                <p class="description">{{$showroom->content_ko}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin: 0 auto;text-align: center">
                <a href="{{route('mobile.index')}}" class="btn btn-primary"
                   style="height: auto;background: #204181;color: #fff;width: 100px; -webkit-border-radius: 3rem; -moz-border-radius: 3rem;border-radius: 3rem;margin-top:3em">Back</a>
            </div>
        </section>
    </main>
@endsection