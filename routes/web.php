<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/mail', function () {
    return view('mail.mail');
});
Route::get('/api-menu','MenuController@index')->name('api.menu');

Route::get('/search', 'SearchController@search')->name('search');
Route::get('change-language/{locale}', 'LanguageController@changeLanguage')->name('user.change-language');
//New and event
Route::get('/', 'CompanyController@index')->name('homepage');
Route::get('/new', 'PostController@index')->name("new.index");
Route::get('/event', 'PostController@event')->name('client.event.index');
Route::get('/event/{id}', 'PostController@showEvent')->name('event.show');
Route::get('/new/{id}/{slug}', 'PostController@showNew');
Route::get('/api/new', 'PostController@getDataModal')->name('api.new');

// download

Route::post('/download','PostController@download')->name('download');

//page
Route::get('/about', 'Controller@about')->name('about.index');
Route::get('/support', 'Controller@support')->name('support');
Route::get('/asean', 'Controller@asean')->name('asean');
//end page

Route::get('/api/search-event-year','PostController@apiShowEvent')->name('apiShowEvent');

//b2b search
Route::get('/search-b2b', 'SearchController@searchB2b')->name('searchB2b');
//new search
Route::get('/search-new', 'SearchController@searchNew')->name('searchNew');
Route::get('/search-event', 'SearchController@searchEvent')->name('searchEvent');

//showroom
Route::get('/search-showroom', 'SearchController@searchShowroom')->name('search.showroom');

//Route company
Route::get('/search-company', 'SearchController@searchCompany')->name('searchCompany');

Route::get('/share-room','Controller@shareroom')->name('client.share-room');
//mobile
Route::group(['prefix' => 'mobile'], function () {
    Route::get('/', 'MobileController@index')->name('mobile.index');
    Route::get('/showroom/{id}', 'MobileController@showroom')->name('mobile.showroom.detail');
    Route::get('/showroom', 'MobileController@showroomAll')->name('mobile.showroom');
    Route::get('/new', 'MobileController@showNew')->name('mobile.new');
    Route::get('/event', 'MobileController@showEvent')->name('mobile.event');
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/book', 'MobileController@showBook')->name('mobile.book');
    });
    Route::get('/company/{id}', 'MobileController@showDetailCompany')->name('mobile.company.detail');
    Route::get('/company/', 'MobileController@showCompany')->name('mobile.company');

});
//company
Route::get('/company', 'CompanyController@showList')->name('client.company.index');
Route::get('/company/{id}', 'CompanyController@show');
Route::get('/company-incubating', 'CompanyController@showIncubating')->name('company.incubating');
Route::get('/company-b2b', 'CompanyController@showB2b')->name('company.b2b');
Route::get('/company-all', 'CompanyController@showCompanyAll')->name('company.company');
//showroom
Route::get('/showroom', 'ShowroomController@index')->name('client.showroom.index');
Route::get('/showroom/{id}', 'ShowroomController@show')->name('show.detail');
//only login
Route::group(['middleware' => 'auth'], function () {
    //book
    Route::get('/api/book', 'BookController@apiShowListBook')->name('client.book.show');
    Route::get('/api/book-pdf', 'BookController@apiShowListBookPDF')->name('client.book.pdf');
    //show

    Route::get('/book/{id}', 'BookController@show')->name('client.book.index');
    Route::get('/book-lager', 'BookController@show1')->name('client.book-lager');
    Route::get('/book-small', 'BookController@show2')->name('client.book-small');
    Route::get('/book-open', 'BookController@show3')->name('client.book-open');
    //end
    Route::get('/client-book', 'BookController@book')->name('client.book.new');
    Route::get('/api/confirm-book', 'BookController@confirm')->name('book.confirm');
    Route::get('/user-account', 'BookController@bookHistory')->name('user.profile');
    Route::get('/user-update', 'UserController@update')->name('user.update');
    Route::post('/user-upload', 'UserController@uploadUser')->name('user-upload');


    //export pdf
    Route::get('/export-pdf','PdfController@index')->name('export-pdf');

});
Route::get('/booking-cancer/{id}', 'BookController@deleteBooking')->name('deleteBooking');
//only admin
Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin', 'AdminCompanyController@showAdmin');
    Route::group(['prefix' => 'admin'], function () {
        Route::resource('post', 'AdminPostController');
        //list post
        Route::get('admin/event', 'AdminPostController@listEvent')->name('admin.event.list');
        Route::get('admin/history', 'AdminPostController@listHistory')->name('admin.history.list');

        Route::get('event/{event}/edit', 'AdminPostController@updateEvent')->name('event.edit');
        Route::resource('company', 'AdminCompanyController');

        Route::resource('showroom', 'AdminShowroomController');

        Route::resource('shareroom', 'AdminShareroomController');
        Route::get('event/create', 'AdminPostController@createEvent')->name('event.create');
        Route::get('count', 'AdminCompanyController@count')->name('admin.count');
        Route::get('book', 'AdminBookController@index')->name('book.index');
        Route::get('show-book-admin', 'BookController@showBackEnd')->name('book.admin');

        Route::get('/user-admin', 'UserController@list')->name('admin.user');
        Route::post('/user-change-pass', 'UserController@changePass')->name('admin.changepass');

        ///  IMPORT EXPORT
        Route::get('/export/post', 'PostImportExportController@export')->name('admin.post.export');
        Route::post('/import/post', 'PostImportExportController@importNew')->name('admin.post.importNew');

        //new
        Route::get('/post-show-new', 'AdminPostController@showDataNew')->name('admin.post.show-new');
        Route::get('/post-show-history', 'AdminPostController@showDataHistory')->name('admin.post.show-history');
        Route::get('/post-show-event', 'AdminPostController@showDataEvent')->name('admin.post.show-event');

        //company
        Route::get('/api-company-incubating-', 'AdminCompanyController@showDataIncubating')->name('api-company-incubating');
        Route::get('/api-company-b2b', 'AdminCompanyController@showDataB2b')->name('api-company-b2b');
        Route::get('/admin-b2b','AdminCompanyController@B2b')->name('admin.b2b');

        //showroom
        Route::get('/api-showroom','AdminShowroomController@showData')->name('api.showroom');

        //api book list admin
        Route::get('/api/book-list-admin','AdminBookController@showListBook')->name('api.showListBook');

        Route::resource('/admin-book','AdminBookController');

        ///user
        Route::get('/api-list-user','UserController@showDataUser')->name('api.list-user');

        // menu admin
        Route::get('/admin-menu',"AdminMenuController@index")->name('admin.menu');
        Route::get('/admin-menu-update',"AdminMenuController@update")->name('admin.menu-update');
    });
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
//language



